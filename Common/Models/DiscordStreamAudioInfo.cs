﻿using System.IO;
using System.Net;
using Vbot.IO;

namespace Vbot.Models
{
    public class DiscordStreamAudioInfo : DiscordAudioInfo
    {
        private readonly int port;

        public DiscordStreamAudioInfo(int port)
        {
            this.port = port;
        }

        public int BufferSize => 1436;

        public Stream CreateStream()
        {
            return new VoiceMeeterStream(IPAddress.Any, port);
        }
    }
}
