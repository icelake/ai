﻿using System.IO;
using Vbot.IO;

namespace Vbot.Models
{
    public class DiscordFileAudioInfo : DiscordAudioInfo
    {
        private readonly string path;

        public DiscordFileAudioInfo(string path)
        {
            this.path = path;
        }

        public int BufferSize => 81920;

        public Stream CreateStream()
        {
            return new FfmpegStream(path);
        }
    }
}
