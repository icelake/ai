﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vbot.Models
{
    public interface DiscordAudioDownloadInfo
    {
        string Url { get; }
        string ExtraArgs { get; }
    }
}
