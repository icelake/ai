﻿namespace Vbot.Models
{
    public class DiscordUrlAudioDownloadInfo : DiscordAudioDownloadInfo
    {
        public string Url { get; set; }
        public string ExtraArgs { get; set; }
    }
}
