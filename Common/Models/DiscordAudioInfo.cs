﻿using System.IO;

namespace Vbot.Models
{
    public interface DiscordAudioInfo
    {
        int BufferSize { get; }
        Stream CreateStream();
    }
}
