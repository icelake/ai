﻿using System.IO;

namespace Vbot.Models
{
    public class DiscordRawFileAudioInfo : DiscordAudioInfo
    {
        private readonly string path;

        public DiscordRawFileAudioInfo(string path)
        {
            this.path = path;
        }

        public int BufferSize => 81920;

        public Stream CreateStream()
        {
            return new FileStream(path, FileMode.Open);
        }
    }
}
