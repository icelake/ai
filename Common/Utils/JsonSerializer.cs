﻿using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Vbot.Utils
{
    public static class JsonSerializer
    {
        public static string Serialize<T>(T obj)
        {
            using (var ms = new MemoryStream())
            using (var sr = new StreamReader(ms))
            {
                var serializer = new DataContractJsonSerializer(typeof(T));
                serializer.WriteObject(ms, obj);
                ms.Position = 0;
                return sr.ReadToEnd();
            }
        }

        public static T Deserialize<T>(Stream stream)
        {
            var serializer = new DataContractJsonSerializer(typeof(T));
            return (T)serializer.ReadObject(stream);
        }

        public static T Deserialize<T>(string str)
        {
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            {
                var serializer = new DataContractJsonSerializer(typeof(T));
                return (T)serializer.ReadObject(ms);
            }
        }
    }
}
