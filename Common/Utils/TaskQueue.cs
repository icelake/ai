﻿using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Vbot.Utils
{
    public class TaskQueue
    {
        private readonly ILogger logger;
        private CancellationTokenSource currentCancellationTokenSource = null;

        public TaskQueue(ILogger<TaskQueue> logger)
        {
            this.logger = logger;
        }

        public void Next()
        {
            currentCancellationTokenSource?.Cancel();
        }

        public async Task ExecuteAsync(Channel<Func<CancellationToken, Task>> channel, CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                var task = await channel.Reader.ReadAsync(cancellationToken);

                try
                {
                    currentCancellationTokenSource = new CancellationTokenSource();
                    using (var linkedCts = CancellationTokenSource.CreateLinkedTokenSource(currentCancellationTokenSource.Token, cancellationToken))
                    {
                        await task(linkedCts.Token);
                    }
                }
                catch (OperationCanceledException)
                {
                    // pass
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, ex.Message);
                }
                finally
                {
                    currentCancellationTokenSource?.Dispose();
                    currentCancellationTokenSource = null;
                }
            }
        }
    }
}
