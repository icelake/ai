﻿using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Vbot.Utils
{
    public class TaskLoop
    {
        private readonly ILogger logger;
        private CancellationTokenSource currentCancellationTokenSource = null;

        public TaskLoop(ILogger<TaskLoop> logger)
        {
            this.logger = logger;
        }

        public void Next()
        {
            currentCancellationTokenSource?.Cancel();
        }

        public async Task ExecuteAsync(Func<CancellationToken, Task> task, CancellationToken cancellationToken)
        {
            if (task == null)
            {
                throw new ArgumentNullException("Task is null");
            }
            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    currentCancellationTokenSource = new CancellationTokenSource();
                    using (var linkedCts = CancellationTokenSource.CreateLinkedTokenSource(currentCancellationTokenSource.Token, cancellationToken))
                    {
                        await task(linkedCts.Token);
                    }
                }
                catch (OperationCanceledException)
                {
                    // pass
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, ex.Message);
                }
                finally
                {
                    currentCancellationTokenSource = null;
                }
            }
        }
    }
}
