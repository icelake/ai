﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Vbot.IO
{
    public class VoiceMeeterStream : Stream
    {
        private static readonly int[] VBanBitResolutionSize =
        {
            1, 2, 3, 4, 4, 8
        };

        private readonly UdpClient client;

        public VoiceMeeterStream(IPAddress address, int port)
        {
            client = new UdpClient(new IPEndPoint(address, port));
        }

        public override bool CanRead => true;

        public override bool CanSeek => false;

        public override bool CanWrite => false;

        public override long Length => throw new NotImplementedException();

        public override long Position { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                client.Dispose();
            }
        }

        public override void Flush()
        {
            throw new NotImplementedException();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            if (count < 1436)
            {
                throw new ArgumentException($"Invalid count {count}");
            }

            while (true)
            {
                var receiveResult = client.ReceiveAsync().Result;

                var receiveBuffer = receiveResult.Buffer;
                if (receiveBuffer[0] != 0x56 || receiveBuffer[1] != 0x42 || receiveBuffer[2] != 0x41 || receiveBuffer[3] != 0x4E)
                {
                    throw new FormatException("magic number");
                }
                var formatSampleRate = receiveBuffer[4];
                if ((formatSampleRate & 0xE0) != 0x00)
                {
                    throw new FormatException("protocol");
                }
                formatSampleRate &= 0x1F;

                var formatSample = receiveBuffer[5];
                var formatChannel = receiveBuffer[6];

                var formatBit = receiveBuffer[7];
                if ((formatBit & 0x80) != 0)
                {
                    throw new FormatException("format bit");
                }
                if ((formatBit & 0xF0) != 0x00)
                {
                    throw new FormatException("codec");
                }

                var size = (formatSample + 1) * (formatChannel + 1) * VBanBitResolutionSize[formatBit & 0x07];
                if (size != (receiveBuffer.Length - 28))
                {
                    throw new FormatException("size");
                }

                Array.Copy(receiveBuffer, 28, buffer, 0, size);
                for (int i = 0; i < size; ++i)
                {
                    if (buffer[i] != 0x00)
                    {
                        return size;
                    }
                }
            }
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }
    }
}
