﻿using System;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Vbot.Extensions
{
    public static class ReactiveObservableExtensions
    {
        private static class Stubs<T>
        {
            public static Action<T> Ignore = _ => { };
        }
        
        public static IDisposable SubscribeAsync<T>(this IObservable<T> source, Func<Task> onNextAsync, Action<Exception> onError)
        {
            return source
                .Select(item => Observable.FromAsync(onNextAsync).Catch((Exception ex) =>
                {
                    onError(ex);
                    return Observable.Empty<Unit>();
                }))
                .Concat()
                .Subscribe(Stubs<Unit>.Ignore, onError);
        }

        public static IDisposable SubscribeAsync<T>(this IObservable<T> source, Func<T, Task> onNextAsync, Action<Exception> onError)
        {
            return source
                .Select(item => Observable.FromAsync(() => onNextAsync(item)).Catch((Exception ex) =>
                {
                    onError(ex);
                    return Observable.Empty<Unit>();
                }))
                .Concat()
                .Subscribe(Stubs<Unit>.Ignore, onError);
        }

        public static IDisposable SubscribeAsync<T1, T2>(this IObservable<(T1, T2)> source, Func<T1, T2, Task> onNextAsync, Action<Exception> onError)
        {
            return source
                .Select(item => Observable.FromAsync(() => onNextAsync(item.Item1, item.Item2)).Catch((Exception ex) =>
                {
                    onError(ex);
                    return Observable.Empty<Unit>();
                }))
                .Concat()
                .Subscribe(Stubs<Unit>.Ignore, onError);
        }

        public static IDisposable SubscribeCancellableAsync<T>(this IObservable<T> source, Func<CancellationToken, Task> onNextAsync, Action<Exception> onError)
        {
            return source
                .Select(item => Observable.FromAsync((token) => onNextAsync(token)).Catch((Exception ex) =>
                {
                    onError(ex);
                    return Observable.Empty<Unit>();
                }))
                .Concat()
                .Subscribe(Stubs<Unit>.Ignore, onError);
        }

        public static IDisposable SubscribeAsync<T1, T2, T3>(this IObservable<(T1, T2, T3)> source, Func<T1, T2, T3, Task> onNextAsync, Action<Exception> onError)
        {
            return source
                .Select(item => Observable.FromAsync(() => onNextAsync(item.Item1, item.Item2, item.Item3)).Catch((Exception ex) =>
                {
                    onError(ex);
                    return Observable.Empty<Unit>();
                }))
                .Concat()
                .Subscribe(Stubs<Unit>.Ignore, onError);
        }

        public static IObservable<T> StepInterval<T>(this IObservable<T> source, TimeSpan delay)
        {
            return source.Select(x =>
                Observable.Empty<T>()
                    .Delay(delay)
                    .StartWith(x)
            ).Concat();
        }
    }
}
