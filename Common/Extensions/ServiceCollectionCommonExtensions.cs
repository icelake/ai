﻿using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Vbot.AppOptions;
using Vbot.Clients;
using Vbot.Services;
using Vbot.UseCases;
using Vbot.Utils;

namespace Vbot.Extensions
{
    public static class ServiceCollectionCommonExtensions
    {
        public static void AddCommonDiscord(this IServiceCollection services, IConfiguration configuration)
        {
            // configure
            services.Configure<DiscordClientOptions>(configuration.GetSection("DiscordClient"));
            services.Configure<DiscordAudioOptions>(configuration.GetSection("DiscordAudio"));

            // frameworks
            services.AddTaskQueue();
            services.AddSingleton<DiscordSocketClient>();
            services.AddSingleton<CommandService>();

            // clients
            services.AddSingleton<IDiscordMessageClient, DiscordMessageClient>();
            services.AddSingleton<IDiscordAudioClient, DiscordAudioClient>();
            services.AddSingleton<IYoutubeDlClient, YoutubeDlClient>();

            // services
            services.AddHostedService<DiscordService>();

            // usecases
            services.AddSingleton<IDiscordClientUseCase, DiscordClientUseCase>();
        }

        public static void AddWebhookDiscord(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<DiscordWebhookOptions>(configuration.GetSection("DiscordWebhook"));
            services.AddSingleton<IDiscordWebhookClient, DiscordWebhookClient>();
        }

        public static void AddTaskLoop(this IServiceCollection services)
        {
            services.AddTransient<TaskLoop>();
        }

        public static void AddTaskQueue(this IServiceCollection services)
        {
            services.AddTransient<TaskQueue>();
        }
    }
}
