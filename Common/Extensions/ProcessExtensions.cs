﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Vbot.Extensions
{
    public static class ProcessExtensions
    {
        public static Task<int> StartAsync(this Process process, CancellationToken cancellationToken)
        {
            var tcs = new TaskCompletionSource<int>();

            cancellationToken.Register(() =>
            {
                if (!process.HasExited)
                {
                    process.Kill();
                }
            });
            process.Exited += (sender, args) =>
            {
                tcs.SetResult(process.ExitCode);
            };
            if (!process.Start())
            {
                throw new InvalidOperationException("Process not started");
            }

            return tcs.Task;
        }

        public static async Task<string> ReadStandardOutputAsync(this Process process)
        {
            using (var stream = process.StandardOutput)
            {
                return await stream.ReadToEndAsync();
            }
        }

        public static async Task<string> ReadStandardErrorAsync(this Process process)
        {
            using (var stream = process.StandardError)
            {
                return await stream.ReadToEndAsync();
            }
        }
    }
}
