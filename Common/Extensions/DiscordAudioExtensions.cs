﻿using Discord.WebSocket;
using System.Collections.Generic;

namespace Vbot.Extensions
{
    public static class DiscordAudioExtensions
    {
        public static SocketVoiceChannel MaxUserJoined(this IEnumerable<SocketVoiceChannel> channels)
        {
            var maxUserCount = 0;
            SocketVoiceChannel selectedChannel = null;
            foreach (var channel in channels)
            {
                var count = channel.Users.Count;
                if (maxUserCount >= count)
                {
                    continue;
                }
                maxUserCount = count;
                selectedChannel = channel;
            }
            return selectedChannel;
        }
    }
}
