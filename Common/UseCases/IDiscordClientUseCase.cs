﻿using System.Threading;
using System.Threading.Tasks;

namespace Vbot.UseCases
{
    public interface IDiscordClientUseCase
    {
        Task StartAsync(CancellationToken cancellationToken);
        Task StopAsync(CancellationToken cancellationToken);
        Task ExecuteAsync(CancellationToken cancellationToken);
    }
}
