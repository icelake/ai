﻿using System.Threading;
using System.Threading.Tasks;
using Vbot.Clients;

namespace Vbot.UseCases
{
    public class DiscordClientUseCase : IDiscordClientUseCase
    {
        private readonly IDiscordMessageClient client;
        private readonly IDiscordAudioClient audioClient;

        public DiscordClientUseCase(
            IDiscordMessageClient client,
            IDiscordAudioClient audioClient)
        {
            this.client = client;
            this.audioClient = audioClient;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            return client.StartAsync();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return client.StopAsync();
        }

        public Task ExecuteAsync(CancellationToken cancellationToken)
        {
            return audioClient.ExecuteAsync(cancellationToken);
        }
    }
}
