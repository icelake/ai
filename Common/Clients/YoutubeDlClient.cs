﻿using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Vbot.Extensions;
using Vbot.Utils;

namespace Vbot.Clients
{
    public class YoutubeDlClient : IYoutubeDlClient
    {
        private const string DirectoryName = "temp";

        public async Task<string> DownloadAudioAsync(CancellationToken cancellationToken, string url, string extraArgs)
        {
            Directory.CreateDirectory(DirectoryName);

            if (extraArgs == null)
            {
                extraArgs = "";
            }
            using (var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "youtube-dl",
                    Arguments = $"-o \"{DirectoryName}{Path.DirectorySeparatorChar}%(extractor)s-%(id)s.%(ext)s\" -f bestaudio/best --print-json {extraArgs} \"{url}\"",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true
                },
                EnableRaisingEvents = true
            })
            {
                var processTask = process.StartAsync(cancellationToken);
                var readOutputTask = process.ReadStandardOutputAsync();
                var readErrorTask = process.ReadStandardErrorAsync();

                var exitCode = await processTask;
                var output = await readOutputTask;
                var error = await readErrorTask;

                var outputObj = JsonSerializer.Deserialize<OutputArgument>(output);
                if (exitCode != 0)
                {
                    throw new IOException(error);
                }
                var fileName = outputObj.FileName;
                if (!File.Exists(fileName))
                {
                    throw new FileNotFoundException(fileName);
                }
                return fileName;
            };
        }

        [DataContract]
        private class OutputArgument
        {
            [DataMember(Name = "extractor")]
            public string Extractor { get; set; }

            [DataMember(Name = "id")]
            public string Id { get; set; }

            [DataMember(Name = "_filename")]
            public string FileName { get; set; }
        }
    }
}
