﻿using System.Threading;
using System.Threading.Tasks;

namespace Vbot.Clients
{
    public interface IYoutubeDlClient
    {
        Task<string> DownloadAudioAsync(CancellationToken cancellationToken, string url, string extraArgs);
    }
}
