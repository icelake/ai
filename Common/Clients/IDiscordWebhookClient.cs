﻿using Discord;
using System.Threading.Tasks;

namespace Vbot.Clients
{
    public interface IDiscordWebhookClient
    {
        Task SendMessageAsync(string message, string username, string avatarUrl);
        void SendMessageQueue(string message, string username, string avatarUrl);
        bool IsTargetChannel(IChannel channel);
    }
}
