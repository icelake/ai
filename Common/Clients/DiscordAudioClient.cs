﻿using Discord.Audio;
using Discord.WebSocket;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using Vbot.AppOptions;
using Vbot.Models;
using Vbot.Utils;

namespace Vbot.Clients
{
    public class DiscordAudioClient : IDiscordAudioClient
    {
        private readonly DiscordAudioOptions discordAudioOptions;
        private readonly Channel<Func<CancellationToken, Task>> playChannel = Channel.CreateUnbounded<Func<CancellationToken, Task>>();
        private readonly Channel<Func<CancellationToken, Task>> downloadChannel = Channel.CreateUnbounded<Func<CancellationToken, Task>>();
        private readonly TaskQueue playQueue;
        private readonly TaskQueue downloadQueue;
        private readonly IYoutubeDlClient youtubeDlClient;

        public DiscordAudioClient(
            IOptions<DiscordAudioOptions> discordAudioOptions,
            TaskQueue playQueue,
            TaskQueue downloadQueue,
            IYoutubeDlClient youtubeDlClient)
        {
            this.discordAudioOptions = discordAudioOptions.Value;
            this.playQueue = playQueue;
            this.downloadQueue = downloadQueue;
            this.youtubeDlClient = youtubeDlClient;
        }

        public Task ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.WhenAll(
                playQueue.ExecuteAsync(playChannel, cancellationToken),
                downloadQueue.ExecuteAsync(downloadChannel, cancellationToken));
        }

        public void EnqueueRawFile(SocketVoiceChannel voiceChannel, string path)
        {
            EnqueueAudio(voiceChannel, new DiscordRawFileAudioInfo(path));
        }

        public void EnqueueStream(SocketVoiceChannel voiceChannel)
        {
            EnqueueAudio(voiceChannel, new DiscordStreamAudioInfo(discordAudioOptions.VoiceMeeterPort));
        }

        public void EnqueueUrl(SocketVoiceChannel voiceChannel, string url, string extraArgs = "")
        {
            EnqueueDownload(voiceChannel, new DiscordUrlAudioDownloadInfo
            {
                Url = url,
                ExtraArgs = extraArgs
            });
        }

        private void EnqueueAudio(SocketVoiceChannel voiceChannel, DiscordAudioInfo info)
        {
            playChannel.Writer.TryWrite((token) => PlayAsync(voiceChannel, info, token));
        }

        private void EnqueueDownload(SocketVoiceChannel voiceChannel, DiscordAudioDownloadInfo info)
        {
            downloadChannel.Writer.TryWrite((token) => DownloadAsync(voiceChannel, info, token));
        }

        public void Next()
        {
            playQueue.Next();
        }

        private async Task PlayAsync(SocketVoiceChannel voiceChannel, DiscordAudioInfo info, CancellationToken cancellationToken)
        {
            using (var stream = info.CreateStream())
            using (var audioClient = await voiceChannel.ConnectAsync())
            using (var audioStream = audioClient.CreatePCMStream(AudioApplication.Music))
            {
                await stream.CopyToAsync(audioStream, info.BufferSize, cancellationToken);
                await audioStream.FlushAsync(cancellationToken);
            }
        }

        private async Task DownloadAsync(SocketVoiceChannel voiceChannel, DiscordAudioDownloadInfo info, CancellationToken cancellationToken)
        {
            using (var tokenSource = new CancellationTokenSource(TimeSpan.FromMinutes(30)))
            using (var token = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken, tokenSource.Token))
            {
                var path = await youtubeDlClient.DownloadAudioAsync(token.Token, info.Url, info.ExtraArgs);
                EnqueueAudio(voiceChannel, new DiscordFileAudioInfo(path));
            }
        }
    }
}
