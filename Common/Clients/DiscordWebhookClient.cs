﻿using Discord;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using Vbot.AppOptions;
using Vbot.Extensions;

namespace Vbot.Clients
{
    public class DiscordWebhookClient : IDiscordWebhookClient
    {
        private readonly ILogger logger;
        private readonly Discord.Webhook.DiscordWebhookClient client;
        private readonly DiscordWebhookOptions discordWebhookOptions;

        private readonly Subject<(string, string, string)> sendMessageSubject = new Subject<(string, string, string)>();

        public DiscordWebhookClient(
            ILogger<DiscordWebhookClient> logger,
            IOptions<DiscordWebhookOptions> discordWebhookOptions)
        {
            this.logger = logger;
            var options = discordWebhookOptions.Value;
            this.discordWebhookOptions = options;
            client = new Discord.Webhook.DiscordWebhookClient(options.Id, options.Token);

            sendMessageSubject
                .StepInterval(TimeSpan.FromSeconds(1))
                .ObserveOn(ThreadPoolScheduler.Instance)
                .SubscribeAsync(SendMessageAsync, OnError);
        }

        private void OnError(Exception ex)
        {
            logger.LogError(ex, ex.Message);
        }

        public Task SendMessageAsync(string message, string username, string avatarUrl)
        {
            return client.SendMessageAsync(message, username: username, avatarUrl: avatarUrl);
        }

        public void SendMessageQueue(string message, string username, string avatarUrl)
        {
            sendMessageSubject.OnNext((message, username, avatarUrl));
        }

        public bool IsTargetChannel(IChannel channel)
        {
            return discordWebhookOptions.Channel == channel.Id;
        }
    }
}
