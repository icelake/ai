﻿using Discord.WebSocket;
using System.Threading;
using System.Threading.Tasks;

namespace Vbot.Clients
{
    public interface IDiscordAudioClient
    {
        Task ExecuteAsync(CancellationToken cancellationToken);
        void Next();
        void EnqueueRawFile(SocketVoiceChannel voiceChannel, string path);
        void EnqueueStream(SocketVoiceChannel voiceChannel);
        void EnqueueUrl(SocketVoiceChannel voiceChannel, string url, string extraArgs = "");
    }
}
