﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Reflection;
using System.Threading.Tasks;
using Vbot.AppOptions;
using Vbot.Extensions;

namespace Vbot.Clients
{
    public class DiscordMessageClient : IDiscordMessageClient
    {
        private readonly ILogger logger;
        private readonly DiscordClientOptions discordClientOptions;
        private readonly IServiceProvider services;
        private readonly DiscordSocketClient socketClient;
        private readonly CommandService commandService;

        private readonly BehaviorSubject<Unit> readySubject = new BehaviorSubject<Unit>(Unit.Default);
        private readonly Subject<SocketUserMessage> receiveMessageSubject = new Subject<SocketUserMessage>();
        private readonly Subject<(Cacheable<IMessage, ulong>, SocketMessage, ISocketMessageChannel)> receiveMessageUpdateSubject = new Subject<(Cacheable<IMessage, ulong>, SocketMessage, ISocketMessageChannel)>();
        private readonly Subject<(Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction)> receiveReactionSubject = new Subject<(Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction)>();

        private readonly Subject<(ISocketMessageChannel, string)> sendMessageSubject = new Subject<(ISocketMessageChannel, string)>();
        private readonly Subject<(SocketUserMessage, IEmote)> sendReactionSubject = new Subject<(SocketUserMessage, IEmote)>();
        private readonly Subject<(IGuildUser, IRole, bool)> sendRoleSubject = new Subject<(IGuildUser, IRole, bool)>();

        public IObservable<SocketUserMessage> MessageReceived => receiveMessageSubject.AsObservable();
        public IObservable<(Cacheable<IMessage, ulong>, SocketMessage, ISocketMessageChannel)> MessageUpdated => receiveMessageUpdateSubject.AsObservable();
        public IObservable<Unit> Ready => readySubject.AsObservable();
        public IObservable<(Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction)> ReactionAdded => receiveReactionSubject.AsObservable();

        public SocketSelfUser CurrentUser => socketClient.CurrentUser;

        public DiscordMessageClient(
            ILogger<DiscordMessageClient> logger,
            IOptions<DiscordClientOptions> discordClientOptions,
            DiscordSocketClient socketClient,
            CommandService commandService,
            IServiceProvider services)
        {
            this.logger = logger;
            this.discordClientOptions = discordClientOptions.Value;
            this.services = services;
            this.socketClient = socketClient;
            this.commandService = commandService;

            socketClient.Ready += ReadyInternal;
            socketClient.MessageReceived += MessageReceivedInternal;
            socketClient.MessageUpdated += MessageUpdatedInternal;
            socketClient.ReactionAdded += ReactionAddedInternal;
            socketClient.Log += Log;

            readySubject
                .ObserveOn(ThreadPoolScheduler.Instance);
            receiveMessageSubject
                .ObserveOn(ThreadPoolScheduler.Instance);
            receiveMessageUpdateSubject
                .ObserveOn(ThreadPoolScheduler.Instance);
            receiveReactionSubject
                .ObserveOn(ThreadPoolScheduler.Instance);

            sendMessageSubject
                .StepInterval(TimeSpan.FromSeconds(1))
                .ObserveOn(ThreadPoolScheduler.Instance)
                .SubscribeAsync(SendMessageAsync, OnError);
            sendReactionSubject
                .StepInterval(TimeSpan.FromSeconds(1))
                .ObserveOn(ThreadPoolScheduler.Instance)
                .SubscribeAsync(AddReactionAsync, OnError);
            sendRoleSubject
                .StepInterval(TimeSpan.FromSeconds(1))
                .ObserveOn(ThreadPoolScheduler.Instance)
                .SubscribeAsync((IGuildUser user, IRole role, bool present) =>
                {
                    if (present)
                        return AddRoleAsync(user, role);
                    else
                        return RemoveRoleAsync(user, role);
                }, OnError);
        }
        
        public void Dispose()
        {
            socketClient.Dispose();
            readySubject.Dispose();
            receiveMessageSubject.Dispose();
            receiveReactionSubject.Dispose();
        }

        public async Task StartAsync()
        {
            var token = discordClientOptions.Token;
            if (token == null)
            {
                throw new NullReferenceException("Discord token not specified");
            }
            await commandService.AddModulesAsync(Assembly.GetEntryAssembly());
            await socketClient.LoginAsync(TokenType.Bot, token);
            await socketClient.StartAsync();
        }

        public async Task StopAsync()
        {
            await socketClient.LogoutAsync();
            await socketClient.StopAsync();
        }

        private void OnError(Exception ex)
        {
            logger.LogError(ex, ex.Message);
        }

        private Task ReadyInternal()
        {
            readySubject.OnCompleted();
            return Task.CompletedTask;
        }

        private Task MessageReceivedInternal(SocketMessage messageParam)
        {
            var message = messageParam as SocketUserMessage;
            if (message != null)
            {
                receiveMessageSubject.OnNext(message);
            }
            return Task.CompletedTask;
        }

        private Task MessageUpdatedInternal(Cacheable<IMessage, ulong> oldMessage, SocketMessage message, ISocketMessageChannel channel)
        {
            receiveMessageUpdateSubject.OnNext((oldMessage, message, channel));
            return Task.CompletedTask;
        }

        private Task ReactionAddedInternal(Cacheable<IUserMessage, ulong> message, ISocketMessageChannel channel, SocketReaction reaction)
        {
            receiveReactionSubject.OnNext((message, channel, reaction));
            return Task.CompletedTask;
        }

        public async Task<bool> ExecuteCommandAsync(SocketUserMessage message)
        {
            int argPos = 0;
            if (!message.Author.IsBot && message.HasStringPrefix(discordClientOptions.Prefix, ref argPos))
            {
                var result = await commandService.ExecuteAsync(new SocketCommandContext(socketClient, message), argPos, services);
                if (!result.IsSuccess)
                {
                    if (result is ExecuteResult executeResult)
                    {
                        logger.LogWarning(executeResult.Exception, result.ErrorReason);
                    }
                    else
                    {
                        logger.LogWarning(result.ErrorReason);
                    }
                }
                return true;
            }
            return false;
        }

        public Task SetActivityAsync(string name)
        {
            return socketClient.SetGameAsync(name);
        }

        public Task SetStatusAsync(UserStatus status)
        {
            return socketClient.SetStatusAsync(status);
        }

        public async Task<bool> SendMessageAsync(ulong id, string text)
        {
            var channel = socketClient.GetChannel(id) as ISocketMessageChannel;
            if (channel == null)
            {
                logger.LogWarning($"Deleted channel found: {id}");
                return false;
            }
            await SendMessageAsync(channel, text);
            return true;
        }

        public bool SendMessageQueue(ulong id, string text)
        {
            var channel = socketClient.GetChannel(id) as ISocketMessageChannel;
            if (channel == null)
            {
                logger.LogWarning($"Deleted channel found: {id}");
                return false;
            }
            sendMessageSubject.OnNext((channel, text));
            return true;
        }

        public async Task SendMessageAsync(ISocketMessageChannel channel, string text)
        {
            await channel.SendMessageAsync(text);
        }

        public void SendMessageQueue(ISocketMessageChannel channel, string text)
        {
            sendMessageSubject.OnNext((channel, text));
        }

        public async Task AddReactionAsync(SocketUserMessage message, IEmote emote)
        {
            await message.AddReactionAsync(emote);
        }

        public void AddReactionQueue(SocketUserMessage message, IEmote emote)
        {
            sendReactionSubject.OnNext((message, emote));
        }

        public Task AddRoleAsync(IGuildUser user, IRole role)
        {
            return user.AddRoleAsync(role);
        }

        public Task AddRoleAsync(IGuildUser user, string name)
        {
            var role = user.Guild.Roles
                .Where(r => r.Name == name)
                .SingleOrDefault();
            if (role == null)
            {
                return Task.CompletedTask;
            }
            return AddRoleAsync(user, role);
        }

        public void AddRoleQueue(IGuildUser user, IRole role)
        {
            sendRoleSubject.OnNext((user, role, true));
        }

        public void AddRoleQueue(IGuildUser user, string name)
        {
            var role = user.Guild.Roles
                .Where(r => r.Name == name)
                .SingleOrDefault();
            if (role == null)
            {
                return;
            }
            AddRoleQueue(user, role);
        }

        public Task RemoveRoleAsync(IGuildUser user, IRole role)
        {
            return user.RemoveRoleAsync(role);
        }

        public Task RemoveRoleAsync(IGuildUser user, string name)
        {
            var role = user.Guild.Roles
                .Where(r => r.Name == name)
                .SingleOrDefault();
            if (role == null)
            {
                return Task.CompletedTask;
            }
            return RemoveRoleAsync(user, role);
        }

        public void RemoveRoleQueue(IGuildUser user, IRole role)
        {
            sendRoleSubject.OnNext((user, role, false));
        }

        public void RemoveRoleQueue(IGuildUser user, string name)
        {
            var role = user.Guild.Roles
                .Where(r => r.Name == name)
                .SingleOrDefault();
            if (role == null)
            {
                return;
            }
            RemoveRoleQueue(user, role);
        }

        private Task Log(LogMessage msg)
        {
            switch (msg.Severity)
            {
                case LogSeverity.Critical:
                    logger.LogCritical(msg.Exception, msg.Message);
                    break;
                case LogSeverity.Debug:
                    logger.LogDebug(msg.Exception, msg.Message);
                    break;
                case LogSeverity.Error:
                    logger.LogError(msg.Exception, msg.Message);
                    break;
                case LogSeverity.Info:
                    logger.LogInformation(msg.Exception, msg.Message);
                    break;
                case LogSeverity.Verbose:
                    logger.LogTrace(msg.Exception, msg.Message);
                    break;
                case LogSeverity.Warning:
                    logger.LogWarning(msg.Exception, msg.Message);
                    break;
                default:
                    logger.LogTrace(msg.Exception, msg.Message);
                    break;
            }
            return Task.CompletedTask;
        }
    }
}
