﻿using Discord;
using Discord.WebSocket;
using System;
using System.Reactive;
using System.Threading.Tasks;

namespace Vbot.Clients
{
    public interface IDiscordMessageClient : IDisposable
    {
        IObservable<SocketUserMessage> MessageReceived { get; }
        IObservable<(Cacheable<IMessage, ulong>, SocketMessage, ISocketMessageChannel)> MessageUpdated { get; }
        IObservable<Unit> Ready { get; }
        IObservable<(Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction)> ReactionAdded { get; }
        SocketSelfUser CurrentUser { get; }

        Task StartAsync();
        Task StopAsync();

        Task<bool> ExecuteCommandAsync(SocketUserMessage message);

        Task SetActivityAsync(string name);
        Task SetStatusAsync(UserStatus status);

        Task<bool> SendMessageAsync(ulong id, string text);
        bool SendMessageQueue(ulong id, string text);
        Task SendMessageAsync(ISocketMessageChannel channel, string text);
        void SendMessageQueue(ISocketMessageChannel channel, string text);
        Task AddReactionAsync(SocketUserMessage message, IEmote emote);
        void AddReactionQueue(SocketUserMessage message, IEmote emote);
        Task AddRoleAsync(IGuildUser user, IRole role);
        Task AddRoleAsync(IGuildUser user, string name);
        void AddRoleQueue(IGuildUser user, IRole role);
        void AddRoleQueue(IGuildUser user, string name);
        Task RemoveRoleAsync(IGuildUser user, IRole role);
        Task RemoveRoleAsync(IGuildUser user, string name);
        void RemoveRoleQueue(IGuildUser user, IRole role);
        void RemoveRoleQueue(IGuildUser user, string name);
    }
}
