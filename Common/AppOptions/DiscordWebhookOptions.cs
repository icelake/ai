﻿namespace Vbot.AppOptions
{
    public class DiscordWebhookOptions
    {
        public ulong Id { get; set; }
        public string Token { get; set; }
        public ulong Channel { get; set; }
    }
}
