﻿namespace Vbot.AppOptions
{
    public class DiscordClientOptions
    {
        public string Token { get; set; }
        public string Prefix { get; set; }
    }
}
