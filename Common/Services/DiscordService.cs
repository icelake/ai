﻿using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;
using Vbot.UseCases;

namespace Vbot.Services
{
    public class DiscordService : BackgroundService
    {
        private readonly IDiscordClientUseCase discordClientUseCase;

        public DiscordService(IDiscordClientUseCase discordClientUseCase)
        {
            this.discordClientUseCase = discordClientUseCase;
        }

        public override async Task StartAsync(CancellationToken cancellationToken)
        {
            await discordClientUseCase.StartAsync(cancellationToken);
            await base.StartAsync(cancellationToken);
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            await discordClientUseCase.StopAsync(cancellationToken);
            await base.StopAsync(cancellationToken);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return discordClientUseCase.ExecuteAsync(stoppingToken);
        }
    }
}
