﻿namespace Vbot.Models
{
    public class ShowroomLiveFeed : IFeed
    {
        public string Title { get; set; }
        public string ChannelId { get; set; }
        public long Published { get; set; }
        public bool IsLive => true;
        public string Url => $"https://www.showroom-live.com/{ChannelId}";
    }
}
