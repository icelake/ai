﻿namespace Vbot.Models
{
    public interface IFeed
    {
        string Title { get; }
        string Url { get; }
        bool IsLive { get; }
    }
}
