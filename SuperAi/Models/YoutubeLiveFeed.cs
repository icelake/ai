﻿namespace Vbot.Models
{
    public class YoutubeLiveFeed : IFeed
    {
        public string Title { get; set; }
        public string VideoId { get; set; }
        public bool IsLive => true;
        public string Url => $"https://www.youtube.com/watch?v={VideoId}";
    }
}
