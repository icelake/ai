﻿namespace Vbot.Models
{
    public class NicoliveFeed : IFeed
    {
        public string Title { get; set; }
        public bool IsLive => true;
        public string VideoId { get; set; }
        public string Url => $"http://live.nicovideo.jp/watch/lv{VideoId}";
    }
}
