﻿namespace Vbot.Models
{
    public class NicovideoFeed : IFeed
    {
        public string Title { get; set; }
        public bool IsLive => false;
        public string VideoId { get; set; }
        public string Url => $"http://www.nicovideo.jp/watch/{VideoId}";
    }
}
