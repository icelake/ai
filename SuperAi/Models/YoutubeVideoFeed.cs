﻿namespace Vbot.Models
{
    public class YoutubeVideoFeed : IFeed
    {
        public string Title { get; set; }
        public string VideoId { get; set; }
        public bool IsLive => false;
        public string Url => $"https://www.youtube.com/watch?v={VideoId}";
    }
}
