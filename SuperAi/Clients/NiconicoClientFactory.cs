﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Vbot.Clients
{
    public class NiconicoClientFactory : INiconicoClientFactory
    {
        private readonly IServiceProvider serviceProvider;

        public NiconicoClientFactory(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public INiconicoClient CreateClient()
        {
            return serviceProvider.GetService<INiconicoClient>();
        }
    }
}
