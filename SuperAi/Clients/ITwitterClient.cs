﻿using CoreTweet.Streaming;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Vbot.Clients
{
    public interface ITwitterClient : IDisposable
    {
        IObservable<StatusMessage> GetUserStream(List<long> ids);
        Task<long> GetUserIdFromScreenName(string name);
    }
}
