﻿using System.Net.Http;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Vbot.Models;
using Vbot.Utils;

namespace Vbot.Clients
{
    public class ShowroomClient : IShowroomClient
    {
        private readonly IHttpClientFactory httpClientFactory;

        public ShowroomClient(IHttpClientFactory httpClientFactory)
        {
            this.httpClientFactory = httpClientFactory;
        }

        public void Dispose()
        {
        }

        public async Task<ShowroomLiveFeed> GetLiveAsync(string channel)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                var response = await httpClient.GetAsync($"https://www.showroom-live.com/api/room/status?room_url_key={channel}");

                ShowroomStatusModel roomStatus = null;
                using (var stream = await response.Content.ReadAsStreamAsync())
                {
                    roomStatus = JsonSerializer.Deserialize<ShowroomStatusModel>(stream);
                }

                if (roomStatus == null || !roomStatus.IsLive || !roomStatus.StartedAt.HasValue)
                    return null;

                return new ShowroomLiveFeed
                {
                    Title = roomStatus.RoomName,
                    Published = roomStatus.StartedAt.Value * 10000000L + 621355968000000000L,
                    ChannelId = channel
                };
            }
        }

        [DataContract]
        private class ShowroomStatusModel
        {
            [DataMember(Name = "room_name")]
            public string RoomName { get; set; }

            [DataMember(Name = "is_live")]
            public bool IsLive { get; set; }

            [DataMember(Name = "started_at")]
            public long? StartedAt { get; set; }
        }
    }
}
