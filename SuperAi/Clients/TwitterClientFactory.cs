﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Vbot.Clients
{
    public class TwitterClientFactory : ITwitterClientFactory
    {
        private readonly IServiceProvider serviceProvider;

        public TwitterClientFactory(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public ITwitterClient CreateClient()
        {
            return serviceProvider.GetService<ITwitterClient>();
        }
    }
}
