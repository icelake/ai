﻿using System;
using System.Threading.Tasks;
using Vbot.Models;

namespace Vbot.Clients
{
    public interface IYoutubeHttpClient : IDisposable
    {
        Task<YoutubeLiveFeed> GetLiveAsync(string channel);
    }
}
