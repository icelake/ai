﻿using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Vbot.AppOptions;

namespace Vbot.Clients
{
    public class YoutubeSubscribeClient : IYoutubeSubscribeClient
    {
        private readonly ServerOptions serverOptions;
        private readonly PshbOptions pshbOptions;
        private readonly IHttpClientFactory httpClientFactory;

        public YoutubeSubscribeClient(
            IOptions<ServerOptions> serverOptions,
            IOptions<PshbOptions> pshbOptions,
            IHttpClientFactory httpClientFactory)
        {
            this.serverOptions = serverOptions.Value;
            this.pshbOptions = pshbOptions.Value;
            this.httpClientFactory = httpClientFactory;
        }

        public void Dispose()
        {
        }

        public async Task SubscribeAsync(string channel)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                var httpResult = await httpClient.PostAsync(
                    "https://pubsubhubbub.appspot.com/subscribe",
                    new FormUrlEncodedContent(new Dictionary<string, string>
                    {
                        { "hub.mode", "subscribe" },
                        { "hub.topic", "https://www.youtube.com/xml/feeds/videos.xml?channel_id=" + channel },
                        { "hub.callback", serverOptions.Url + "/youtube/callback" },
                        { "hub.verify", "sync" },
                        { "hub.verify_token", pshbOptions.VerifyToken },
                        { "hub.secret", pshbOptions.SecretKey }
                    }));
                httpResult.EnsureSuccessStatusCode();
            }
        }

        public async Task UnsubscribeAsync(string channel)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                var httpResult = await httpClient.PostAsync(
                "https://pubsubhubbub.appspot.com/subscribe",
                new FormUrlEncodedContent(new Dictionary<string, string>
                {
                        { "hub.mode", "unsubscribe" },
                        { "hub.topic", "https://www.youtube.com/xml/feeds/videos.xml?channel_id=" + channel },
                        { "hub.callback", serverOptions.Url + "/youtube/callback" },
                        { "hub.verify", "sync" },
                        { "hub.verify_token", pshbOptions.VerifyToken },
                        { "hub.secret", pshbOptions.SecretKey }
                }));
                httpResult.EnsureSuccessStatusCode();
            }
        }
    }
}
