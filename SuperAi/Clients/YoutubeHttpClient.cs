﻿using System.Net.Http;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Vbot.Models;
using Vbot.Utils;

namespace Vbot.Clients
{
    public class YoutubeHttpClient : IYoutubeHttpClient
    {
        private static readonly Regex[] Patterns = {
            new Regex(@";ytplayer\.config\s*=\s*({.*?});ytplayer", RegexOptions.Compiled),
            new Regex(@";ytplayer\.config\s*=\s*({.*?});", RegexOptions.Compiled)
        };

        private readonly IHttpClientFactory httpClientFactory;

        public YoutubeHttpClient(IHttpClientFactory httpClientFactory)
        {
            this.httpClientFactory = httpClientFactory;
        }

        public void Dispose()
        {
        }

        private async Task<PlayerConfig> GetJson(string channel)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                var response = await httpClient.GetAsync($"https://www.youtube.com/channel/{channel}/live");
                var body = await response.Content.ReadAsStringAsync();
                foreach (var pattern in Patterns)
                {
                    var match = pattern.Match(body);
                    if (!match.Success)
                        continue;
                    return JsonSerializer.Deserialize<PlayerConfig>(match.Groups[1].Value);
                }
                return null;
            }
        }

        public async Task<YoutubeLiveFeed> GetLiveAsync(string channel)
        {
            var config = await GetJson(channel);

            var videoId = config?.Args?.VideoId ?? null;
            if (videoId == null)
                return null;

            var name = config?.Args?.Title ?? null;
            if (name == null)
                return null;

            return new YoutubeLiveFeed
            {
                VideoId = videoId,
                Title = name
            };
        }

        [DataContract]
        private class PlayerConfig
        {
            [DataContract]
            public class PlayerConfigArgs
            {
                [DataMember(Name = "video_id")]
                public string VideoId { get; set; }

                [DataMember(Name = "title")]
                public string Title { get; set; }
            }

            [DataMember(Name = "args")]
            public PlayerConfigArgs Args { get; set; }
        }
    }
}
