﻿using Google.Apis.YouTube.v3.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Vbot.Clients
{
    public interface IYoutubeApiClient : IDisposable
    {
        Task<Video> ReceiveLiveStreamingDetailsAsync(string videoId);
        Task<IList<Video>> ReceiveLiveStreamingDetailsAsync(List<string> videoIds);
    }
}
