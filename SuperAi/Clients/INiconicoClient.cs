﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Vbot.Models;

namespace Vbot.Clients
{
    public interface INiconicoClient : IDisposable
    {
        IObservable<Nicolive> ReceiveLive { get; }
        IObservable<Nicovideo> ReceiveVideo { get; }
        Task ExecuteAsync(CancellationToken cancellationToken);
        Task<NicoliveFeed> GetLiveFeedAsync(Nicolive live);
        Task<NicovideoFeed> GetVideoFeedAsync(Nicovideo video);
    }
}
