﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Sockets;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Vbot.AppOptions;
using Vbot.Models;
using Vbot.Repositories;

namespace Vbot.Clients
{
    public class NiconicoClient : INiconicoClient
    {
        private readonly Subject<Nicolive> liveSubject = new Subject<Nicolive>();
        private readonly Subject<Nicovideo> videoSubject = new Subject<Nicovideo>();
        public IObservable<Nicolive> ReceiveLive => liveSubject.AsObservable();
        public IObservable<Nicovideo> ReceiveVideo => videoSubject.AsObservable();

        private readonly NiconicoOptions niconicoOptions;
        private readonly HttpClient httpClient;

        public NiconicoClient(
            IOptions<NiconicoOptions> niconicoOptions,
            IHttpClientFactory httpClientFactory)
        {
            this.niconicoOptions = niconicoOptions.Value;
            this.httpClient = httpClientFactory.CreateClient();
        }

        public void Dispose()
        {
            httpClient.Dispose();
            videoSubject.Dispose();
            liveSubject.Dispose();
        }

        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            var xmlDocument = new XmlDocument();

            var response = await httpClient.PostAsync(
                "https://secure.nicovideo.jp/secure/login?site=nicoalert",
                new FormUrlEncodedContent(new Dictionary<string, string>
                {
                    { "mail", niconicoOptions.Mail },
                    { "password", niconicoOptions.Password }
                }));
            xmlDocument.LoadXml(await response.Content.ReadAsStringAsync());

            response = await httpClient.PostAsync(
                "http://alert.nicovideo.jp/front/getalertstatus",
                new FormUrlEncodedContent(new Dictionary<string, string>
                {
                    { "ticket", xmlDocument["nicovideo_user_response"]["ticket"].InnerText }
                }));
            xmlDocument.LoadXml(await response.Content.ReadAsStringAsync());

            var address = xmlDocument["getalertstatus"]["ms"]["addr"].InnerText;
            var port = Convert.ToInt32(xmlDocument["getalertstatus"]["ms"]["port"].InnerText);

            response = await httpClient.PostAsync(
                "https://secure.nicovideo.jp/secure/login?site=niconico",
                new FormUrlEncodedContent(new Dictionary<string, string>
                {
                    { "mail", niconicoOptions.Mail },
                    { "password", niconicoOptions.Password }
                }));

            using (var tcpClient = new TcpClient())
            {
                await tcpClient.ConnectAsync(address, port);
                using (var networkStream = new NiconicoAlertStreamReader(tcpClient.GetStream()))
                {
                    await networkStream.WriteAsync("<thread thread=\"1000000001\" version=\"20061206\" res_from=\"-1\" scores=\"1\"/>\0");
                    await networkStream.WriteAsync("<thread thread=\"1000000002\" version=\"20061206\" res_from=\"-1\" scores=\"1\"/>\0");

                    while (!cancellationToken.IsCancellationRequested)
                    {
                        xmlDocument.LoadXml(await networkStream.ReadAsync());
                        if (xmlDocument.FirstChild.Name != "chat")
                            continue;
                        var thread = xmlDocument.FirstChild.Attributes["thread"].Value;
                        var line = xmlDocument.InnerText;
                        if (thread == "1000000001")
                        {
                            var liveInfo = line.Split(",", 3);
                            if (liveInfo.Length < 3)
                                continue;
                            var live = new Nicolive
                            {
                                VideoId = liveInfo[0],
                                UserId = liveInfo[2]
                            };
                            liveSubject.OnNext(live);
                        }
                        else if (thread == "1000000002")
                        {
                            var videoInfo = line.Split(",", 2);
                            if (videoInfo.Length < 2)
                                continue;
                            var video = new Nicovideo
                            {
                                VideoId = videoInfo[0],
                                UserId = videoInfo[1]
                            };
                            videoSubject.OnNext(video);
                        }
                    }
                }
            }
        }

        public async Task<NicoliveFeed> GetLiveFeedAsync(Nicolive live)
        {
            var liveId = live.VideoId;
            var response = await httpClient.GetAsync("http://live.nicovideo.jp/api/getplayerstatus/lv" + liveId);

            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(await response.Content.ReadAsStringAsync());
            var streamInfo = xmlDocument["getplayerstatus"]["stream"];

            return new NicoliveFeed
            {
                VideoId = liveId,
                Title = streamInfo["owner_name"].InnerText
            };
        }

        public async Task<NicovideoFeed> GetVideoFeedAsync(Nicovideo video)
        {
            var videoId = video.VideoId;
            var response = await httpClient.GetAsync("http://ext.nicovideo.jp/api/getthumbinfo/" + videoId);

            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(await response.Content.ReadAsStringAsync());
            var streamInfo = xmlDocument["nicovideo_thumb_response"]["thumb"];

            return new NicovideoFeed
            {
                VideoId = videoId,
                Title = streamInfo["user_nickname"].InnerText
            };
        }

        public class NiconicoAlertStreamReader : IDisposable
        {
            private const int Capacity = 1024;

            private readonly byte[] readBuffer = new byte[Capacity];
            private string readBufferStr = "";
            private readonly NetworkStream stream;

            public NiconicoAlertStreamReader(NetworkStream stream)
            {
                this.stream = stream;
            }

            public void Dispose()
            {
                stream.Dispose();
            }

            public async Task WriteAsync(string str)
            {
                var writeBuffer = Encoding.ASCII.GetBytes(str);
                await stream.WriteAsync(writeBuffer, 0, writeBuffer.Length);
            }

            public async Task<string> ReadAsync()
            {
                var index = readBufferStr.IndexOf("\0", StringComparison.Ordinal);
                while (index < 0)
                {
                    int length = await stream.ReadAsync(readBuffer, 0, readBuffer.Length);
                    if (length <= 0)
                        continue;
                    readBufferStr += Encoding.ASCII.GetString(readBuffer, 0, length);
                    index = readBufferStr.IndexOf("\0", StringComparison.Ordinal);
                }
                var resultStr = readBufferStr.Substring(0, index);
                readBufferStr = readBufferStr.Substring(index + 1);
                return resultStr;
            }
        }
    }
}
