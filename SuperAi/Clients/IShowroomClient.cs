﻿using System;
using System.Threading.Tasks;
using Vbot.Models;

namespace Vbot.Clients
{
    public interface IShowroomClient : IDisposable
    {
        Task<ShowroomLiveFeed> GetLiveAsync(string channel);
    }
}
