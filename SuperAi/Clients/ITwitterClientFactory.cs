﻿namespace Vbot.Clients
{
    public interface ITwitterClientFactory
    {
        ITwitterClient CreateClient();
    }
}
