﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Vbot.Clients
{
    public class YoutubeHttpClientFactory : IYoutubeHttpClientFactory
    {
        private readonly IServiceProvider serviceProvider;

        public YoutubeHttpClientFactory(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public IYoutubeHttpClient CreateClient()
        {
            return serviceProvider.GetService<IYoutubeHttpClient>();
        }
    }
}
