﻿using CoreTweet;
using CoreTweet.Streaming;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Vbot.AppOptions;

namespace Vbot.Clients
{
    public class TwitterClient : ITwitterClient
    {
        private readonly Tokens tokens;

        public TwitterClient(IOptions<TwitterOptions> twitterOptions)
        {
            var options = twitterOptions.Value;
            tokens = Tokens.Create(options.Key, options.Secret, options.Token, options.TokenSecret);
        }

        public void Dispose()
        {
        }

        public async Task<long> GetUserIdFromScreenName(string name)
        {
            var user = await tokens.Users.ShowAsync(name);
            if (!user.Id.HasValue)
            {
                throw new ArgumentException($"Screen name not found:{name}");
            }
            return user.Id.Value;
        }

        public IObservable<StatusMessage> GetUserStream(List<long> ids)
        {
            if (ids.Count == 0)
                return null;

            return tokens.Streaming
                .FilterAsObservable(follow: ids)
                .OfType<StatusMessage>()
                .DefaultIfEmpty(null);
        }
    }
}
