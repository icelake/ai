﻿namespace Vbot.Clients
{
    public interface IYoutubeApiClientFactory
    {
        IYoutubeApiClient CreateClient();
    }
}
