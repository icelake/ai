﻿using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vbot.AppOptions;
using Vbot.Extensions;

namespace Vbot.Clients
{
    public class YoutubeApiClient : IYoutubeApiClient
    {
        private readonly ILogger logger;
        private readonly YouTubeService youtubeService;

        public YoutubeApiClient(
            ILogger<YoutubeApiClient> logger,
            IOptions<GoogleOptions> googleOptions)
        {
            this.logger = logger;
            youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                ApiKey = googleOptions.Value.ApiKey
            });
        }

        public void Dispose()
        {
            youtubeService.Dispose();
        }

        public async Task<Video> ReceiveLiveStreamingDetailsAsync(string videoId)
        {
            var streamRequest = youtubeService.Videos.List("liveStreamingDetails");
            streamRequest.Id = videoId;
            streamRequest.Fields = "items(id,liveStreamingDetails(activeLiveChatId,actualStartTime,actualEndTime))";
            var streamResponse = await streamRequest.ExecuteAsync();

            if (streamResponse.Items.Count == 0)
            {
                return null;
            }
            return streamResponse.Items[0];
        }

        public async Task<IList<Video>> ReceiveLiveStreamingDetailsAsync(List<string> videoIds)
        {
            var items = new List<Video>();
            foreach (var ids in videoIds.Split(49))
            {
                var idsStr = string.Join(",", ids);
                var streamRequest = youtubeService.Videos.List("liveStreamingDetails");
                streamRequest.Id = idsStr;
                streamRequest.Fields = "items(id,liveStreamingDetails(activeLiveChatId,actualStartTime,actualEndTime))";
                var streamResponse = await streamRequest.ExecuteAsync();

                if (streamResponse.Items.Count == 0)
                {
                    continue;
                }
                items.AddRange(streamResponse.Items);
            }
            return items;
        }
    }
}
