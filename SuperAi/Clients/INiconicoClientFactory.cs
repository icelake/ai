﻿namespace Vbot.Clients
{
    public interface INiconicoClientFactory
    {
        INiconicoClient CreateClient();
    }
}
