﻿namespace Vbot.Clients
{
    public interface IShowroomClientFactory
    {
        IShowroomClient CreateClient();
    }
}
