﻿namespace Vbot.Clients
{
    public interface IYoutubeSubscribeClientFactory
    {
        IYoutubeSubscribeClient CreateClient();
    }
}
