﻿using System;
using System.Threading.Tasks;

namespace Vbot.Clients
{
    public interface IYoutubeSubscribeClient : IDisposable
    {
        Task SubscribeAsync(string channel);
        Task UnsubscribeAsync(string channel);
    }
}
