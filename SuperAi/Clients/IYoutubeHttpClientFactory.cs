﻿namespace Vbot.Clients
{
    public interface IYoutubeHttpClientFactory
    {
        IYoutubeHttpClient CreateClient();
    }
}
