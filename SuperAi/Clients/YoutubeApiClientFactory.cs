﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Vbot.Clients
{
    public class YoutubeApiClientFactory : IYoutubeApiClientFactory
    {
        private readonly IServiceProvider serviceProvider;

        public YoutubeApiClientFactory(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public IYoutubeApiClient CreateClient()
        {
            return serviceProvider.GetService<IYoutubeApiClient>();
        }
    }
}
