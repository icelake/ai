﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Vbot.Clients
{
    public class ShowroomClientFactory : IShowroomClientFactory
    {
        private readonly IServiceProvider serviceProvider;

        public ShowroomClientFactory(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public IShowroomClient CreateClient()
        {
            return serviceProvider.GetService<IShowroomClient>();
        }
    }
}
