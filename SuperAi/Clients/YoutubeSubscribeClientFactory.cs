﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Vbot.Clients
{
    public class YoutubeSubscribeClientFactory : IYoutubeSubscribeClientFactory
    {
        private readonly IServiceProvider serviceProvider;

        public YoutubeSubscribeClientFactory(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public IYoutubeSubscribeClient CreateClient()
        {
            return serviceProvider.GetService<IYoutubeSubscribeClient>();
        }
    }
}
