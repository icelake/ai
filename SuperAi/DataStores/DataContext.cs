﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Vbot.DataModels;

namespace Vbot.DataStores
{
    public class DataContext : DbContext
    {
        public DbSet<DiscordChannel> DiscordChannels { get; set; }

        public DbSet<YoutubeChannel> YoutubeChannels { get; set; }
        public DbSet<YoutubeVideo> YoutubeVideos { get; set; }
        public DbSet<DiscordYoutube> DiscordYoutubes { get; set; }

        public DbSet<NicoliveChannel> NicoliveChannels { get; set; }
        public DbSet<DiscordNicolive> DiscordNicolives { get; set; }

        public DbSet<NicovideoChannel> NicovideoChannels { get; set; }
        public DbSet<DiscordNicovideo> DiscordNicovideos { get; set; }

        public DbSet<ShowroomChannel> ShowroomChannels { get; set; }
        public DbSet<DiscordShowroom> DiscordShowrooms { get; set; }

        public DbSet<TwitterUser> TwitterUsers { get; set; }
        public DbSet<DiscordTwitter> DiscordTwitters { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
            // ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DiscordChannel>()
                .HasIndex(b => b.Channel)
                .IsUnique();

            modelBuilder.Entity<YoutubeChannel>()
                .HasIndex(b => b.Channel)
                .IsUnique();
            modelBuilder.Entity<DiscordYoutube>()
                .HasKey(b => new { b.DiscordId, b.YoutubeId });
            modelBuilder.Entity<DiscordYoutube>()
                .HasOne(b => b.Discord)
                .WithMany(b => b.Youtube)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(b => b.DiscordId);
            modelBuilder.Entity<DiscordYoutube>()
                .HasOne(b => b.Youtube)
                .WithMany(b => b.Discord)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(b => b.YoutubeId);

            modelBuilder.Entity<YoutubeVideo>()
                .HasOne(b => b.Channel)
                .WithOne(b => b.Live)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey<YoutubeVideo>(b => b.ChannelId);

            modelBuilder.Entity<NicoliveChannel>()
                .HasIndex(b => b.Channel)
                .IsUnique();
            modelBuilder.Entity<DiscordNicolive>()
                .HasKey(b => new { b.DiscordId, b.NicoliveId });
            modelBuilder.Entity<DiscordNicolive>()
                .HasOne(b => b.Discord)
                .WithMany(b => b.Nicolive)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(b => b.DiscordId);
            modelBuilder.Entity<DiscordNicolive>()
                .HasOne(b => b.Nicolive)
                .WithMany(b => b.Discord)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(b => b.NicoliveId);

            modelBuilder.Entity<NicovideoChannel>()
                .HasIndex(b => b.Channel)
                .IsUnique();
            modelBuilder.Entity<DiscordNicovideo>()
                .HasKey(b => new { b.DiscordId, b.NicovideoId });
            modelBuilder.Entity<DiscordNicovideo>()
                .HasOne(b => b.Discord)
                .WithMany(b => b.Nicovideo)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(b => b.DiscordId);
            modelBuilder.Entity<DiscordNicovideo>()
                .HasOne(b => b.Nicovideo)
                .WithMany(b => b.Discord)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(b => b.NicovideoId);

            modelBuilder.Entity<ShowroomChannel>()
                .HasIndex(b => b.Channel)
                .IsUnique();
            modelBuilder.Entity<DiscordShowroom>()
                .HasKey(b => new { b.DiscordId, b.ShowroomId });
            modelBuilder.Entity<DiscordShowroom>()
                .HasOne(b => b.Discord)
                .WithMany(b => b.Showroom)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(b => b.DiscordId);
            modelBuilder.Entity<DiscordShowroom>()
                .HasOne(b => b.Showroom)
                .WithMany(b => b.Discord)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(b => b.ShowroomId);

            modelBuilder.Entity<TwitterUser>()
                .HasIndex(b => new { b.UserId, b.Pattern })
                .IsUnique();
            modelBuilder.Entity<DiscordTwitter>()
                .HasKey(b => new { b.DiscordId, b.TwitterId });
            modelBuilder.Entity<DiscordTwitter>()
                .HasOne(b => b.Discord)
                .WithMany(b => b.Twitter)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(b => b.DiscordId);
            modelBuilder.Entity<DiscordTwitter>()
                .HasOne(b => b.Twitter)
                .WithMany(b => b.Discord)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(b => b.TwitterId);
        }
    }
}
