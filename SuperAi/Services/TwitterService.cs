﻿using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;
using Vbot.UseCases.Services;

namespace Vbot.Services
{
    public class TwitterService : BackgroundService
    {
        private readonly ITwitterServiceUseCase twitterServiceUseCase;

        public TwitterService(ITwitterServiceUseCase twitterServiceUseCase)
        {
            this.twitterServiceUseCase = twitterServiceUseCase;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return twitterServiceUseCase.ExecuteAsync(stoppingToken);
        }
    }
}
