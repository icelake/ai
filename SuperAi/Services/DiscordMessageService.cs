﻿using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;
using Vbot.UseCases.Services;

namespace Vbot.Services
{
    public class DiscordMessageService : IHostedService
    {
        private readonly IDiscordMessageServiceUseCase discordMessageServiceUseCase;

        public DiscordMessageService(IDiscordMessageServiceUseCase discordMessageServiceUseCase)
        {
            this.discordMessageServiceUseCase = discordMessageServiceUseCase;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            return discordMessageServiceUseCase.StartAsync(cancellationToken);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return discordMessageServiceUseCase.StopAsync(cancellationToken);
        }
    }
}
