﻿using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;
using Vbot.UseCases.Services;

namespace Vbot.Services
{
    public class NiconicoService : BackgroundService
    {
        private readonly INiconicoServiceUseCase niconicoServiceUseCase;

        public NiconicoService(INiconicoServiceUseCase niconicoServiceUseCase)
        {
            this.niconicoServiceUseCase = niconicoServiceUseCase;
        }

        public override Task StartAsync(CancellationToken stoppingToken)
        {
            return niconicoServiceUseCase.StartAsync(stoppingToken);
        }

        public override Task StopAsync(CancellationToken stoppingToken)
        {
            return niconicoServiceUseCase.StopAsync(stoppingToken);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return niconicoServiceUseCase.ExecuteAsync(stoppingToken);
        }
    }
}
