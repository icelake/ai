﻿using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;
using Vbot.UseCases.Services;

namespace Vbot.Services
{
    public class YoutubeService : BackgroundService
    {
        private readonly IYoutubeServiceUseCase youtubeServiceUseCase;

        public YoutubeService(IYoutubeServiceUseCase youtubeServiceUseCase)
        {
            this.youtubeServiceUseCase = youtubeServiceUseCase;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return youtubeServiceUseCase.ExecuteAsync(stoppingToken);
        }
    }
}
