﻿using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;
using Vbot.UseCases.Services;

namespace Vbot.Services
{
    public class ShowroomService : BackgroundService
    {
        private readonly IShowroomServiceUseCase showroomServiceUseCase;

        public ShowroomService(IShowroomServiceUseCase showroomServiceUseCase)
        {
            this.showroomServiceUseCase = showroomServiceUseCase;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return showroomServiceUseCase.ExecuteAsync(stoppingToken);
        }
    }
}
