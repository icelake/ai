﻿using Discord;
using Discord.Commands;
using System.Threading.Tasks;

namespace Vbot.Modules
{
    public class HelpModule : ModuleBase<SocketCommandContext>
    {
        [Command("help")]
        public async Task Help()
        {
            var builder = new EmbedBuilder()
            {
                Color = new Color(247, 203, 196),
                Title = "コマンド一覧"
            };
            builder.AddField("!ai channel list", "登録中の通知を表示する");
            builder.AddField("!ai youtube register チャンネルID", "YouTubeチャンネルの通知をオンにする");
            builder.AddField("!ai youtube unregister チャンネルID", "YouTubeチャンネルの通知をオフにする");
            builder.AddField("!ai nicovideo register ユーザID", "ニコニコ動画の通知をオンにする");
            builder.AddField("!ai nicovideo unregister ユーザID", "ニコニコ動画の通知をオフにする");
            builder.AddField("!ai nicolive register ユーザID", "ニコニコ生放送の通知をオンにする");
            builder.AddField("!ai nicolive unregister ユーザID", "ニコニコ生放送の通知をオフにする");
            builder.AddField("!ai showroom register ユーザID", "SHOWROOMの通知をオンにする");
            builder.AddField("!ai showroom unregister ユーザID", "SHOWROOMの通知をオフにする");
            builder.AddField("!ai twitter register スクリーンネーム", "Twitterの通知をオンにする");
            builder.AddField("!ai twitter unregister スクリーンネーム", "Twitterの通知をオフにする");
            //builder.AddField("!ai twitter mirrativ register スクリーンネーム", "Mirrativの通知をオンにする（Twitterの投稿から通知）");
            await ReplyAsync("Hai domo!:sparkles: **SuperAI** is **S**uper **u**ltimate **p**erfect **e**xcellent **r**eliable **A**rtificial **I**ntelligence :sushi:", embed: builder.Build());
        }
    }
}
