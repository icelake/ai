﻿using Discord;
using Discord.Commands;
using System.Threading.Tasks;
using Vbot.Consts;
using Vbot.UseCases.Modules;

namespace Vbot.Modules
{
    [Group("nicolive")]
    public class NicoliveModule : ModuleBase<SocketCommandContext>
    {
        private readonly INicoliveModuleUseCase nicoliveModuleUseCase;

        public NicoliveModule(INicoliveModuleUseCase nicoliveModuleUseCase)
        {
            this.nicoliveModuleUseCase = nicoliveModuleUseCase;
        }

        [Command("register")]
        public async Task Register(string niconicoId, IChannel specifiedChannel = null)
        {
            try
            {
                await nicoliveModuleUseCase.RegisterAsync((specifiedChannel ?? Context.Message.Channel).Id, niconicoId);
                await Context.Message.AddReactionAsync(EmojiConsts.Succeed);
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
        }

        [Command("unregister")]
        public async Task Unregister(string niconicoId, IChannel specifiedChannel = null)
        {
            try
            {
                await nicoliveModuleUseCase.UnregisterAsync((specifiedChannel ?? Context.Message.Channel).Id, niconicoId);
                await Context.Message.AddReactionAsync(EmojiConsts.Succeed);
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
        }
    }
}
