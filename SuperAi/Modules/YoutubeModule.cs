﻿using Discord;
using Discord.Commands;
using System.Threading.Tasks;
using Vbot.Consts;
using Vbot.UseCases.Modules;

namespace Vbot.Modules
{
    [Group("youtube")]
    public class YoutubeModule : ModuleBase<SocketCommandContext>
    {
        private readonly IYoutubeModuleUseCase youtubeModuleUseCase;

        public YoutubeModule(IYoutubeModuleUseCase youtubeModuleUseCase)
        {
            this.youtubeModuleUseCase = youtubeModuleUseCase;
        }

        [Command("register")]
        public async Task Register(string youtubeId, IChannel specifiedChannel = null)
        {
            try
            {
                await youtubeModuleUseCase.RegisterAsync((specifiedChannel ?? Context.Channel).Id, youtubeId);
                await Context.Message.AddReactionAsync(EmojiConsts.Succeed);
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
        }

        [Command("unregister")]
        public async Task Unregister(string youtubeId, IChannel specifiedChannel = null)
        {
            try
            {
                await youtubeModuleUseCase.UnregisterAsync((specifiedChannel ?? Context.Channel).Id, youtubeId);
                await Context.Message.AddReactionAsync(EmojiConsts.Succeed);
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
        }
    }
}
