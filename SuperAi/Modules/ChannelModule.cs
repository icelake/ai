﻿using Discord;
using Discord.Commands;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vbot.Consts;
using Vbot.UseCases.Modules;

namespace Vbot.Modules
{
    [Group("channel")]
    public class ChannelModule : ModuleBase<SocketCommandContext>
    {
        private readonly IChannelModuleUseCase channelModuleUseCase;

        public ChannelModule(IChannelModuleUseCase channelModuleUseCase)
        {
            this.channelModuleUseCase = channelModuleUseCase;
        }

        [Command("register")]
        public async Task Register(IChannel specifiedChannel = null)
        {
            try
            {
                await channelModuleUseCase.RegisterAsync((specifiedChannel ?? Context.Channel).Id);
                await Context.Message.AddReactionAsync(EmojiConsts.Succeed);
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
        }

        [Command("unregister")]
        public async Task Unregister(IChannel specifiedChannel = null)
        {
            try
            {
                await channelModuleUseCase.UnregisterAsync((specifiedChannel ?? Context.Channel).Id);
                await Context.Message.AddReactionAsync(EmojiConsts.Succeed);
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
        }

        [Command("list")]
        public async Task List(ulong channel = 0)
        {
            if (channel == 0)
            {
                channel = Context.Channel.Id;
            }

            var builder = new EmbedBuilder()
            {
                Color = new Color(247, 203, 196),
                Title = "チャンネル一覧"
            };

            void Show(string title, IEnumerable<object> list)
            {
                var listStr = string.Join("\n", list);
                if (listStr.Length != 0)
                    builder.AddField(title, listStr);
            }

            var discordChannel = await channelModuleUseCase.GetListAsync(channel);
            if (discordChannel == null)
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                return;
            }
            Show("YouTube", discordChannel.Youtube.Select(b => b.Youtube));
            Show("ニコニコ動画", discordChannel.Nicovideo.Select(b => b.Nicovideo));
            Show("ニコニコ生放送", discordChannel.Nicolive.Select(b => b.Nicolive));
            Show("SHOWROOM", discordChannel.Showroom.Select(b => b.Showroom));
            Show("Twitter", discordChannel.Twitter.Select(b => b.Twitter));
            await ReplyAsync("", false, builder.Build());
        }
    }
}
