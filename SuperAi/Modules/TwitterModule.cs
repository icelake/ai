﻿using Discord;
using Discord.Commands;
using System.Threading.Tasks;
using Vbot.Consts;
using Vbot.UseCases.Modules;

namespace Vbot.Modules
{
    [Group("twitter")]
    public class TwitterModule : ModuleBase<SocketCommandContext>
    {
        private readonly ITwitterModuleUseCase twitterModuleUseCase;

        public TwitterModule(ITwitterModuleUseCase twitterModuleUseCase)
        {
            this.twitterModuleUseCase = twitterModuleUseCase;
        }

        [Command("register")]
        public async Task Register(string screenName, IChannel specifiedChannel = null, [Remainder]string pattern = null)
        {
            try
            {
                await twitterModuleUseCase.RegisterAsync((specifiedChannel ?? Context.Message.Channel).Id, screenName, pattern);
                await Context.Message.AddReactionAsync(EmojiConsts.Succeed);
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
        }

        [Command("unregister")]
        public async Task Unregister(string screenName, IChannel specifiedChannel = null, [Remainder]string pattern = null)
        {
            try
            {
                await twitterModuleUseCase.UnregisterAsync((specifiedChannel ?? Context.Message.Channel).Id, screenName, pattern);
                await Context.Message.AddReactionAsync(EmojiConsts.Succeed);
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
        }
    }
}
