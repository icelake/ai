﻿using Discord;
using Discord.Commands;
using System.Threading.Tasks;
using Vbot.Consts;
using Vbot.UseCases.Modules;

namespace Vbot.Modules
{
    [Group("showroom")]
    public class ShowroomModule : ModuleBase<SocketCommandContext>
    {
        private readonly IShowroomModuleUseCase showroomModuleUseCase;

        public ShowroomModule(IShowroomModuleUseCase showroomModuleUseCase)
        {
            this.showroomModuleUseCase = showroomModuleUseCase;
        }

        [Command("register")]
        public async Task Register(string showroomId, IChannel specifiedChannel = null)
        {
            try
            {
                await showroomModuleUseCase.RegisterAsync((specifiedChannel ?? Context.Message.Channel).Id, showroomId);
                await Context.Message.AddReactionAsync(EmojiConsts.Succeed);
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
        }

        [Command("unregister")]
        public async Task Unregister(string showroomId, IChannel specifiedChannel = null)
        {
            try
            {
                await showroomModuleUseCase.UnregisterAsync((specifiedChannel ?? Context.Message.Channel).Id, showroomId);
                await Context.Message.AddReactionAsync(EmojiConsts.Succeed);
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
        }
    }
}
