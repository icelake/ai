﻿using Discord;
using Discord.Commands;
using System.Threading.Tasks;
using Vbot.Consts;
using Vbot.UseCases.Modules;

namespace Vbot.Modules
{
    [Group("nicovideo")]
    public class NicovideoModule : ModuleBase<SocketCommandContext>
    {
        private readonly INicovideoModuleUseCase nicovideoModuleUseCase;

        public NicovideoModule(INicovideoModuleUseCase nicovideoModuleUseCase)
        {
            this.nicovideoModuleUseCase = nicovideoModuleUseCase;
        }

        [Command("register")]
        public async Task Register(string niconicoId, IChannel specifiedChannel = null)
        {
            try
            {
                await nicovideoModuleUseCase.RegisterAsync((specifiedChannel ?? Context.Message.Channel).Id, niconicoId);
                await Context.Message.AddReactionAsync(EmojiConsts.Succeed);
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
        }

        [Command("unregister")]
        public async Task Unregister(string niconicoId, IChannel specifiedChannel = null)
        {
            try
            {
                await nicovideoModuleUseCase.UnregisterAsync((specifiedChannel ?? Context.Message.Channel).Id, niconicoId);
                await Context.Message.AddReactionAsync(EmojiConsts.Succeed);
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
        }
    }
}
