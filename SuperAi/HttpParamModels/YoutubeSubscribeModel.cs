﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Vbot.HttpParamModels
{
    public class YoutubeSubscribeModel
    {
        [FromQuery(Name = "hub.verify_token")]
        [Required]
        public string VerifyToken { get; set; }

        [FromQuery(Name = "hub.topic")]
        [Required]
        public string Topic { get; set; }

        [FromQuery(Name = "hub.challenge")]
        [Required]
        public string Challenge { get; set; }
    }
}
