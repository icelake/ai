﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Vbot.HttpParamModels
{
    public class YoutubeCallbackModel
    {
        [FromBody]
        public byte[] Content { get; set; }

        [FromHeader(Name = "X-Hub-Signature")]
        [Required]
        public string HubSignature { get; set; }
    }
}
