﻿using Microsoft.Extensions.Options;
using System;
using System.Globalization;
using System.Security.Authentication;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Vbot.AppOptions;
using Vbot.Clients;
using Vbot.DataModels;
using Vbot.Enumerations;
using Vbot.Extensions;
using Vbot.HttpParamModels;
using Vbot.Models;
using Vbot.Repositories;

namespace Vbot.UseCases.Controllers
{
    public class YoutubeControllerUseCase : IYoutubeControllerUseCase
    {
        private readonly PshbOptions pshbOptions;
        private readonly IYoutubeRepository youtubeRepository;
        private readonly IYoutubeApiClientFactory youtubeApiClientFactory;
        private readonly IDiscordMessageClient discordClient;

        public YoutubeControllerUseCase(
            IOptions<PshbOptions> pshbOptions,
            IYoutubeRepository youtubeRepository,
            IYoutubeApiClientFactory youtubeApiClientFactory,
            IDiscordMessageClient discordClient)
        {
            this.pshbOptions = pshbOptions.Value;
            this.youtubeRepository = youtubeRepository;
            this.youtubeApiClientFactory = youtubeApiClientFactory;
            this.discordClient = discordClient;
        }

        public Task<string> SubscribeAsync(YoutubeSubscribeModel model)
        {
            if (model.VerifyToken != pshbOptions.VerifyToken)
            {
                throw new AuthenticationException($"Wrong verify token:{model.VerifyToken}");
            }
            if (!model.Topic.StartsWith("https://www.youtube.com/xml/feeds/videos.xml?channel_id="))
            {
                throw new AuthenticationException($"Wrong topic:{model.Topic}");
            }
            return Task.FromResult(model.Challenge);
        }

        public async Task CallbackAsync(YoutubeCallbackModel model)
        {
            var content = model.Content;

            // check signature
            using (var hmac = new HMACSHA1(Encoding.ASCII.GetBytes(pshbOptions.SecretKey)))
            {
                var hash = hmac.ComputeHash(content, 0, content.Length);
                var hashStr = new StringBuilder("sha1=");
                foreach (byte b in hash)
                {
                    var hex = b.ToString("x2");
                    hashStr.Append(hex);
                }
                if (!hashStr.ToString().Equals(model.HubSignature, StringComparison.OrdinalIgnoreCase))
                {
                    throw new AuthenticationException("Wrong hub signature");
                }
            }

            // parse content
            var xml = new XmlDocument();
            xml.LoadXml(Encoding.UTF8.GetString(content));

            var entry = xml["feed"]["entry", "http://www.w3.org/2005/Atom"];
            if (entry == null)
            {
                return;
            }

            var channelId = entry["channelId", "http://www.youtube.com/xml/schemas/2015"].InnerText;
            if (channelId == null)
                throw new Exception("youtube channel id not found");

            var videoId = entry["videoId", "http://www.youtube.com/xml/schemas/2015"].InnerText;
            if (videoId == null)
                throw new Exception("youtube video id not found");

            var videoName = entry["title", "http://www.w3.org/2005/Atom"].InnerText;
            if (videoName == null)
                throw new Exception("youtube video name not found");

            var publishedRaw = entry["published", "http://www.w3.org/2005/Atom"].InnerText;
            if (publishedRaw == null)
                throw new Exception("youtube published time not found");

            var published = DateTime.ParseExact(publishedRaw, "yyyy-MM-ddTHH:mm:sszzz", DateTimeFormatInfo.InvariantInfo, DateTimeStyles.None).ToUniversalTime().Ticks;

            using (var youtubeApiClient = youtubeApiClientFactory.CreateClient())
            {
                var liveDetails = await youtubeApiClient.ReceiveLiveStreamingDetailsAsync(videoId);
                var live = liveDetails.LiveStreamingDetails;
                switch (live.GetVideoType())
                {
                    case YoutubeVideoType.Video:
                        {
                            var channel = await youtubeRepository.UpdateLastNotifyTimeAsync(channelId, published);
                            if (channel != null)
                            {
                                await discordClient.PostFeedAsync(channel.Discord, new YoutubeVideoFeed
                                {
                                    VideoId = videoId,
                                    Title = videoName
                                });
                            }
                        }
                        break;
                    case YoutubeVideoType.PreStartLive:
                        {
                            await youtubeRepository.AddLiveAsync(channelId, new YoutubeVideo
                            {
                                VideoId = videoId,
                                VideoName = videoName
                            });
                        }
                        break;
                    case YoutubeVideoType.InProgressLive:
                        {
                            var channel = await youtubeRepository.PopStartedLiveByChannelAsync(channelId, live.ActualStartTime.Value.ToUniversalTime().Ticks);
                            if (channel != null)
                            {
                                await discordClient.PostFeedAsync(channel.Discord, new YoutubeLiveFeed
                                {
                                    VideoId = videoId,
                                    Title = videoName
                                });
                            }
                        }
                        break;
                    default:
                        await youtubeRepository.RemoveLiveByChannelAsync(channelId);
                        break;
                }
            }
        }
    }
}
