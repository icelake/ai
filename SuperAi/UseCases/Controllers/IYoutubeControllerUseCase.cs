﻿using System.Threading.Tasks;
using Vbot.HttpParamModels;

namespace Vbot.UseCases.Controllers
{
    public interface IYoutubeControllerUseCase
    {
        Task<string> SubscribeAsync(YoutubeSubscribeModel model);
        Task CallbackAsync(YoutubeCallbackModel model);
    }
}
