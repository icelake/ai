﻿using System.Threading;
using System.Threading.Tasks;

namespace Vbot.UseCases.Services
{
    public interface IYoutubeServiceUseCase
    {
        Task ExecuteAsync(CancellationToken cancellationToken);
    }
}
