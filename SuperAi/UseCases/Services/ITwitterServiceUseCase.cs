﻿using System.Threading;
using System.Threading.Tasks;

namespace Vbot.UseCases.Services
{
    public interface ITwitterServiceUseCase
    {
        Task ExecuteAsync(CancellationToken stoppingToken);
    }
}
