﻿using CoreTweet;
using CoreTweet.Streaming;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Reactive.Threading.Tasks;
using System.Threading;
using System.Threading.Tasks;
using Vbot.Clients;
using Vbot.DataModels;
using Vbot.Extensions;
using Vbot.Repositories;
using Vbot.Utils;

namespace Vbot.UseCases.Services
{
    public class TwitterServiceUseCase : ITwitterServiceUseCase
    {
        private const int IntervalReconnectMilliseconds = 60000;

        private readonly ILogger logger;
        private readonly ITwitterClient twitterClient;
        private readonly ITwitterRepository twitterRepository;
        private readonly IDiscordMessageClient discordClient;
        private readonly TaskLoop taskLoop;

        public TwitterServiceUseCase(
            ILogger<TwitterServiceUseCase> logger,
            ITwitterClient twitterClient,
            ITwitterRepository twitterRepository,
            IDiscordMessageClient discordClient,
            TaskLoop taskLoop)
        {
            this.logger = logger;
            this.twitterClient = twitterClient;
            this.twitterRepository = twitterRepository;
            this.discordClient = discordClient;
            this.taskLoop = taskLoop;
        }

        public Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return taskLoop.ExecuteAsync(ObserveAsync, stoppingToken);
        }

        private async Task ObserveAsync(CancellationToken cancellationToken)
        {
            await Task.Delay(IntervalReconnectMilliseconds);

            var observable = twitterClient.GetUserStream(await twitterRepository.GetUserIdsAsync());
            if (observable == null)
            {
                return;
            }
            using (var disposable = observable.Subscribe(MessageReceived, OnError))
            {
                await observable.ToTask(cancellationToken);
            }
        }

        private async void MessageReceived(StatusMessage message)
        {
            if (message == null)
                return;
            var status = message.Status;
            if (status.RetweetedStatus != null)
                return;

            var patternList = await twitterRepository.GetAsync(status.User.Id.Value);

            if (status.ExtendedTweet == null)
            {
                foreach (var item in patternList)
                {
                    if (CheckPattern(item.Pattern, status.Text, status.Entities))
                    {
                        await PostTweetAsync(item, status);
                    }
                }
            }
            else
            {
                var extended = status.ExtendedTweet;
                foreach (var item in patternList)
                {
                    if (CheckPattern(item.Pattern, extended.FullText, extended.Entities))
                    {
                        await PostTweetAsync(item, status);
                    }
                }
            }
        }

        private static bool CheckPattern(string pattern, string text, Entities entities)
        {
            if (string.IsNullOrEmpty(pattern))
                return true;
            return text?.Contains(pattern) ?? entities?.Urls?.Any(b => b.ExpandedUrl.Contains(pattern)) ?? false;
        }

        private Task PostTweetAsync(TwitterUser user, Status message)
        {
            return discordClient.PostStringAsync(user.Discord, $"https://twitter.com/{message.User.ScreenName}/statuses/{message.Id}");
        }

        private void OnError(Exception ex)
        {
            logger.LogError(ex, ex.Message);
        }
    }
}
