﻿using System.Threading;
using System.Threading.Tasks;

namespace Vbot.UseCases.Services
{
    public interface IDiscordMessageServiceUseCase
    {
        Task StartAsync(CancellationToken cancellationToken);
        Task StopAsync(CancellationToken cancellationToken);
    }
}
