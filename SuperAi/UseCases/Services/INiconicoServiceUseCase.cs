﻿using System.Threading;
using System.Threading.Tasks;

namespace Vbot.UseCases.Services
{
    public interface INiconicoServiceUseCase
    {
        Task StartAsync(CancellationToken stoppingToken);
        Task StopAsync(CancellationToken stoppingToken);
        Task ExecuteAsync(CancellationToken cancellationToken);
    }
}
