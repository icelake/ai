﻿using System.Threading;
using System.Threading.Tasks;

namespace Vbot.UseCases.Services
{
    public interface IShowroomServiceUseCase
    {
        Task ExecuteAsync(CancellationToken stoppingToken);
    }
}
