﻿using Discord.WebSocket;
using Microsoft.Extensions.Logging;
using System;
using System.Reactive.Disposables;
using System.Threading;
using System.Threading.Tasks;
using Vbot.Clients;
using Vbot.Extensions;

namespace Vbot.UseCases.Services
{
    public class DiscordMessageServiceUseCase : IDiscordMessageServiceUseCase
    {
        private readonly ILogger logger;
        private readonly IDiscordMessageClient discordClient;
        private CompositeDisposable observer = null;

        public DiscordMessageServiceUseCase(
            ILogger<DiscordMessageServiceUseCase> logger,
            IDiscordMessageClient discordClient)
        {
            this.logger = logger;
            this.discordClient = discordClient;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            observer = new CompositeDisposable(
                discordClient.Ready.SubscribeAsync(Ready, OnError),
                discordClient.MessageReceived.SubscribeAsync(MessageReceived, OnError));
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            observer?.Dispose();
            return Task.CompletedTask;
        }

        private async Task Ready()
        {
            await discordClient.SetActivityAsync("!ai help");
        }

        private void OnError(Exception ex)
        {
            logger.LogError(ex, ex.Message);
        }

        private async Task MessageReceived(SocketUserMessage message)
        {
            await discordClient.ExecuteCommandAsync(message);
        }
    }
}
