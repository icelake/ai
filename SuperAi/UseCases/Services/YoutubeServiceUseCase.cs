﻿using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using Vbot.Clients;
using Vbot.DataModels;
using Vbot.Enumerations;
using Vbot.Extensions;
using Vbot.Models;
using Vbot.Repositories;

namespace Vbot.UseCases.Services
{
    public class YoutubeServiceUseCase : IYoutubeServiceUseCase
    {
        private const int IntervalSubscribeMilliseconds = 86400000;
        private const int IntervalSubscribePerChannelMilliseconds = 60000;
        private const int IntervalCheckLiveMilliseconds = 60000;
        private const int IntervalCheckUrlMilliseconds = 60000;

        private readonly ILogger logger;
        private readonly IYoutubeApiClientFactory youtubeApiClientFactory;
        private readonly IYoutubeHttpClientFactory youtubeHttpClientFactory;
        private readonly IYoutubeSubscribeClientFactory youtubeSubscribeClientFactory;
        private readonly IYoutubeRepository youtubeRepository;
        private readonly IDiscordMessageClient discordClient;

        public YoutubeServiceUseCase(
            ILogger<YoutubeServiceUseCase> logger,
            IYoutubeApiClientFactory youtubeApiClientFactory,
            IYoutubeHttpClientFactory youtubeHttpClientFactory,
            IYoutubeSubscribeClientFactory youtubeSubscribeClientFactory,
            IYoutubeRepository youtubeRepository,
            IDiscordMessageClient discordClient)
        {
            this.logger = logger;
            this.youtubeApiClientFactory = youtubeApiClientFactory;
            this.youtubeHttpClientFactory = youtubeHttpClientFactory;
            this.youtubeSubscribeClientFactory = youtubeSubscribeClientFactory;
            this.youtubeRepository = youtubeRepository;
            this.discordClient = discordClient;
        }

        public Task ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.WhenAll(
                SubscribePshbAsync(cancellationToken),
                CheckLiveAsync(cancellationToken),
                CheckUrlAsync(cancellationToken));
        }

        private async Task SubscribePshbAsync(CancellationToken cancellationToken)
        {
            using (var youtubeSubscribeClient = youtubeSubscribeClientFactory.CreateClient())
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    foreach (var channel in await youtubeRepository.GetChannelIdsAsync())
                    {
                        try
                        {
                            await youtubeSubscribeClient.SubscribeAsync(channel);
                        }
                        catch (Exception ex)
                        {
                            logger.LogError(ex, ex.Message);
                        }
                        await Task.Delay(IntervalSubscribePerChannelMilliseconds, cancellationToken);
                    }
                    await Task.Delay(IntervalSubscribeMilliseconds, cancellationToken);
                }
            }
        }

        private async Task CheckLiveAsync(CancellationToken cancellationToken)
        {
            using (var youtubeApiClient = youtubeApiClientFactory.CreateClient())
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    await Task.Delay(IntervalCheckLiveMilliseconds, cancellationToken);

                    var liveIdList = await youtubeRepository.GetLiveIdsAsync();
                    if (liveIdList.Count == 0)
                    {
                        continue;
                    }
                    var liveDetailsList = await youtubeApiClient.ReceiveLiveStreamingDetailsAsync(liveIdList);
                    if (liveDetailsList == null)
                    {
                        continue;
                    }

                    foreach (var liveDetails in liveDetailsList)
                    {
                        try
                        {
                            var live = liveDetails.LiveStreamingDetails;
                            switch (live.GetVideoType())
                            {
                                case YoutubeVideoType.Video:
                                    await youtubeRepository.RemoveLiveByIdAsync(liveDetails.Id);
                                    break;
                                case YoutubeVideoType.FinishedLive:
                                    await youtubeRepository.RemoveLiveByIdAsync(liveDetails.Id);
                                    break;
                                case YoutubeVideoType.InProgressLive:
                                    var video = await youtubeRepository.PopStartedLiveByIdAsync(liveDetails.Id, live.ActualStartTime.Value.ToUniversalTime().Ticks);
                                    if (video != null)
                                    {
                                        await discordClient.PostFeedAsync(video.Channel.Discord, new YoutubeLiveFeed
                                        {
                                            VideoId = video.VideoId,
                                            Title = video.VideoName
                                        });
                                    }
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.LogError(ex, ex.Message);
                        }
                    }
                }
            }
        }

        private async Task CheckUrlAsync(CancellationToken cancellationToken)
        {
            using (var youtubeHttpClient = youtubeHttpClientFactory.CreateClient())
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    var channels = await youtubeRepository.GetChannelIdsAsync();
                    if (channels.Count == 0)
                    {
                        await Task.Delay(IntervalCheckUrlMilliseconds, cancellationToken);
                        continue;
                    }

                    foreach (var channel in channels)
                    {
                        await Task.Delay(IntervalCheckUrlMilliseconds, cancellationToken);
                        try
                        {
                            var feed = await youtubeHttpClient.GetLiveAsync(channel);
                            if (feed != null)
                            {
                                var video = new YoutubeVideo
                                {
                                    VideoId = feed.VideoId,
                                    VideoName = feed.Title
                                };
                                await youtubeRepository.AddLiveAsync(channel, video);
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.LogError(ex, ex.Message);
                        }
                    }
                }
            }
        }
    }
}
