﻿using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using Vbot.Clients;
using Vbot.Extensions;
using Vbot.Repositories;

namespace Vbot.UseCases.Services
{
    public class ShowroomServiceUseCase : IShowroomServiceUseCase
    {
        private const int IntervalMilliseconds = 60000;

        private readonly ILogger logger;
        private readonly IShowroomRepository showroomRepository;
        private readonly IShowroomClientFactory showroomClientFactory;
        private readonly IDiscordMessageClient discordClient;

        public ShowroomServiceUseCase(
            ILogger<ShowroomServiceUseCase> logger,
            IShowroomRepository showroomRepository,
            IShowroomClientFactory showroomClientFactory,
            IDiscordMessageClient discordClient)
        {
            this.logger = logger;
            this.showroomRepository = showroomRepository;
            this.showroomClientFactory = showroomClientFactory;
            this.discordClient = discordClient;
        }

        public async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            using (var showroomClient = showroomClientFactory.CreateClient())
            {
                while (!stoppingToken.IsCancellationRequested)
                {
                    await Task.Delay(IntervalMilliseconds, stoppingToken);
                    foreach (var channel in await showroomRepository.GetChannelIdsAsync())
                    {
                        try
                        {
                            var live = await showroomClient.GetLiveAsync(channel);
                            if (live == null)
                            {
                                continue;
                            }
                            var channelItem = await showroomRepository.CheckAndUpdateLastNotifyTimeAsync(channel, live.Published);
                            if (channelItem != null)
                            {
                                await discordClient.PostFeedAsync(channelItem.Discord, live);
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.LogError(ex, ex.Message);
                        }
                    }
                }
            }
        }
    }
}
