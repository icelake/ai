﻿using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using Vbot.Clients;
using Vbot.Extensions;
using Vbot.Models;
using Vbot.Repositories;

namespace Vbot.UseCases.Services
{
    public class NiconicoServiceUseCase : INiconicoServiceUseCase
    {
        private readonly ILogger logger;
        private readonly INicoliveRepository nicoliveRepository;
        private readonly INicovideoRepository nicovideoRepository;
        private readonly INiconicoClientFactory niconicoClientFactory;
        private readonly IDiscordMessageClient discordClient;
        private INiconicoClient niconicoClient = null;

        public NiconicoServiceUseCase(
            ILogger<NiconicoServiceUseCase> logger,
            INicoliveRepository nicoliveRepository,
            INicovideoRepository nicovideoRepository,
            INiconicoClientFactory niconicoClientFactory,
            IDiscordMessageClient discordClient)
        {
            this.logger = logger;
            this.nicoliveRepository = nicoliveRepository;
            this.nicovideoRepository = nicovideoRepository;
            this.niconicoClientFactory = niconicoClientFactory;
            this.discordClient = discordClient;
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            niconicoClient = niconicoClientFactory.CreateClient();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            niconicoClient?.Dispose();
            return Task.CompletedTask;
        }

        public Task ExecuteAsync(CancellationToken cancellationToken)
        {
            niconicoClient.ReceiveLive
                .Subscribe(ReceiveLive, OnError);
            niconicoClient.ReceiveVideo
                .Subscribe(ReceiveVideo, OnError);
            return niconicoClient.ExecuteAsync(cancellationToken);
        }

        private async void ReceiveLive(Nicolive live)
        {
            var channel = await nicoliveRepository.GetAsync(live.UserId);
            if (channel == null)
            {
                return;
            }
            var feed = await niconicoClient.GetLiveFeedAsync(live);
            await discordClient.PostFeedAsync(channel.Discord, feed);
        }

        private async void ReceiveVideo(Nicovideo video)
        {
            var channel = await nicovideoRepository.GetAsync(video.UserId);
            if (channel == null)
            {
                return;
            }
            var feed = await niconicoClient.GetVideoFeedAsync(video);
            await discordClient.PostFeedAsync(channel.Discord, feed);
        }

        private void OnError(Exception ex)
        {
            logger.LogError(ex, ex.Message);
        }
    }
}
