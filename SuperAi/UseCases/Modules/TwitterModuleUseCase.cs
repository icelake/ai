﻿using System.Threading.Tasks;
using Vbot.Clients;
using Vbot.Repositories;

namespace Vbot.UseCases.Modules
{
    public class TwitterModuleUseCase : ITwitterModuleUseCase
    {
        private readonly ITwitterClientFactory twitterClientFactory;
        private readonly ITwitterRepository twitterRepository;

        public TwitterModuleUseCase(
            ITwitterClientFactory twitterClientFactory,
            ITwitterRepository twitterRepository)
        {
            this.twitterClientFactory = twitterClientFactory;
            this.twitterRepository = twitterRepository;
        }

        public async Task RegisterAsync(ulong discordChannel, string screenName, string pattern)
        {
            if (!long.TryParse(screenName, out var userId))
            {
                using (var twitterClient = twitterClientFactory.CreateClient())
                {
                    userId = await twitterClient.GetUserIdFromScreenName(screenName);
                }
            }
            await twitterRepository.RegisterAsync(discordChannel, userId, pattern);
        }

        public async Task UnregisterAsync(ulong discordChannel, string screenName, string pattern)
        {
            if (!long.TryParse(screenName, out var userId))
            {
                using (var twitterClient = twitterClientFactory.CreateClient())
                {
                    userId = await twitterClient.GetUserIdFromScreenName(screenName);
                }
            }
            await twitterRepository.UnregisterAsync(discordChannel, userId, pattern);
        }
    }
}
