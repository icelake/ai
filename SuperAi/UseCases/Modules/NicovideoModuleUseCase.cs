﻿using System.Threading.Tasks;
using Vbot.Repositories;

namespace Vbot.UseCases.Modules
{
    public class NicovideoModuleUseCase : INicovideoModuleUseCase
    {
        private readonly INicovideoRepository nicovideoRepository;

        public NicovideoModuleUseCase(INicovideoRepository nicovideoRepository)
        {
            this.nicovideoRepository = nicovideoRepository;
        }

        public Task RegisterAsync(ulong discordChannel, string channel)
        {
            return nicovideoRepository.RegisterAsync(discordChannel, channel);
        }

        public Task UnregisterAsync(ulong discordChannel, string channel)
        {
            return nicovideoRepository.UnregisterAsync(discordChannel, channel);
        }
    }
}
