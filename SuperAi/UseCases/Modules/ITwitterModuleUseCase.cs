﻿using System.Threading.Tasks;

namespace Vbot.UseCases.Modules
{
    public interface ITwitterModuleUseCase
    {
        Task RegisterAsync(ulong discordChannel, string screenName, string pattern);
        Task UnregisterAsync(ulong discordChannel, string screenName, string pattern);
    }
}
