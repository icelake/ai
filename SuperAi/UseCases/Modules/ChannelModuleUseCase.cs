﻿using System.Threading.Tasks;
using Vbot.DataModels;
using Vbot.Repositories;

namespace Vbot.UseCases.Modules
{
    public class ChannelModuleUseCase : IChannelModuleUseCase
    {
        private readonly IDiscordRepository repository;

        public ChannelModuleUseCase(IDiscordRepository repository)
        {
            this.repository = repository;
        }

        public Task RegisterAsync(ulong channel)
        {
            return repository.RegisterAsync(channel);
        }

        public Task UnregisterAsync(ulong channel)
        {
            return repository.UnregisterAsync(channel);
        }

        public Task<DiscordChannel> GetListAsync(ulong channel)
        {
            return repository.GetAsync(channel);
        }
    }
}
