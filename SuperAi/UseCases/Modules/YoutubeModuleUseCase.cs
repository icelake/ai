﻿using System.Threading.Tasks;
using Vbot.Clients;
using Vbot.Repositories;

namespace Vbot.UseCases.Modules
{
    public class YoutubeModuleUseCase : IYoutubeModuleUseCase
    {
        private readonly IYoutubeRepository youtubeRepository;
        private readonly IYoutubeSubscribeClientFactory youtubeSubscribeClientFactory;

        public YoutubeModuleUseCase(
            IYoutubeRepository youtubeRepository,
            IYoutubeSubscribeClientFactory youtubeSubscribeClientFactory)
        {
            this.youtubeRepository = youtubeRepository;
            this.youtubeSubscribeClientFactory = youtubeSubscribeClientFactory;
        }

        public Task RegisterAsync(ulong discordChannel, string channel)
        {
            using (var youtubeSubscribeClient = youtubeSubscribeClientFactory.CreateClient())
            {
                youtubeSubscribeClient.SubscribeAsync(channel);
            }
            return youtubeRepository.RegisterAsync(discordChannel, channel);
        }

        public Task UnregisterAsync(ulong discordChannel, string channel)
        {
            using (var youtubeSubscribeClient = youtubeSubscribeClientFactory.CreateClient())
            {
                youtubeSubscribeClient.UnsubscribeAsync(channel);
            }
            return youtubeRepository.UnregisterAsync(discordChannel, channel);
        }
    }
}
