﻿using System.Threading.Tasks;

namespace Vbot.UseCases.Modules
{
    public interface INicovideoModuleUseCase
    {
        Task RegisterAsync(ulong discordChannel, string channel);
        Task UnregisterAsync(ulong discordChannel, string channel);
    }
}
