﻿using System.Threading.Tasks;
using Vbot.Repositories;

namespace Vbot.UseCases.Modules
{
    public class NicoliveModuleUseCase : INicoliveModuleUseCase
    {
        private readonly INicoliveRepository nicoliveRepository;

        public NicoliveModuleUseCase(INicoliveRepository nicoliveRepository)
        {
            this.nicoliveRepository = nicoliveRepository;
        }

        public Task RegisterAsync(ulong discordChannel, string channel)
        {
            return nicoliveRepository.RegisterAsync(discordChannel, channel);
        }

        public Task UnregisterAsync(ulong discordChannel, string channel)
        {
            return nicoliveRepository.UnregisterAsync(discordChannel, channel);
        }
    }
}
