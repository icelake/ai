﻿using System.Threading.Tasks;
using Vbot.Repositories;

namespace Vbot.UseCases.Modules
{
    public class ShowroomModuleUseCase : IShowroomModuleUseCase
    {
        private readonly IShowroomRepository showroomRepository;

        public ShowroomModuleUseCase(IShowroomRepository showroomRepository)
        {
            this.showroomRepository = showroomRepository;
        }

        public Task RegisterAsync(ulong discordChannel, string channel)
        {
            return showroomRepository.RegisterAsync(discordChannel, channel);
        }

        public Task UnregisterAsync(ulong discordChannel, string channel)
        {
            return showroomRepository.UnregisterAsync(discordChannel, channel);
        }
    }
}
