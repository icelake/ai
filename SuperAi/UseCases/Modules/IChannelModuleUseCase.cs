﻿using System.Threading.Tasks;
using Vbot.DataModels;

namespace Vbot.UseCases.Modules
{
    public interface IChannelModuleUseCase
    {
        Task RegisterAsync(ulong channel);
        Task UnregisterAsync(ulong channel);
        Task<DiscordChannel> GetListAsync(ulong channel);
    }
}
