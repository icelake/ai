﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;
using Vbot.HttpParamModels;
using Vbot.UseCases.Controllers;

namespace Vbot.Controllers
{
    [Route("youtube")]
    public class YoutubeController : Controller
    {
        private readonly ILogger logger;
        private readonly IYoutubeControllerUseCase youtubeControllerUseCase;

        public YoutubeController(
            ILogger<YoutubeController> logger,
            IYoutubeControllerUseCase youtubeControllerUseCase)
        {
            this.logger = logger;
            this.youtubeControllerUseCase = youtubeControllerUseCase;
        }

        [HttpGet("callback")]
        public async Task<IActionResult> Callback(YoutubeSubscribeModel model)
        {
            if (!ModelState.IsValid)
            {
                PrintModelError();
                return BadRequest();
            }
            try
            {
                string token = await youtubeControllerUseCase.SubscribeAsync(model);
                return Content(token);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, ex.Message);
            }
            return BadRequest();
        }

        [HttpPost("callback")]
        public async Task<IActionResult> Callback(YoutubeCallbackModel model)
        {
            if (!ModelState.IsValid)
            {
                PrintModelError();
                return BadRequest();
            }
            try
            {
                await youtubeControllerUseCase.CallbackAsync(model);
                return Ok();
            }
            catch (Exception ex)
            {
                logger.LogError(ex, ex.Message);
            }
            return BadRequest();
        }

        private void PrintModelError()
        {
            foreach (var error in ModelState.Values.SelectMany(v => v.Errors))
            {
                logger.LogError(error.Exception, error.ErrorMessage);
            }
        }
    }
}
