﻿using Microsoft.AspNetCore.Mvc.Formatters;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Vbot.Formatters
{
    public class RawBodyInputFormatter : InputFormatter
    {
        private const int Capacity = 1024;

        public override Boolean CanRead(InputFormatterContext context)
        {
            return true;
        }

        public override async Task<InputFormatterResult> ReadRequestBodyAsync(InputFormatterContext context)
        {
            using (var stream = new MemoryStream(Capacity))
            {
                await context.HttpContext.Request.Body.CopyToAsync(stream);
                return await InputFormatterResult.SuccessAsync(stream.ToArray());
            }
        }
    }
}
