﻿namespace Vbot.AppOptions
{
    public class TwitterOptions
    {
        public string Key { get; set; }
        public string Secret { get; set; }
        public string Token { get; set; }
        public string TokenSecret { get; set; }
    }
}
