﻿namespace Vbot.AppOptions
{
    public class PshbOptions
    {
        public string VerifyToken { get; set; }
        public string SecretKey { get; set; }
    }
}
