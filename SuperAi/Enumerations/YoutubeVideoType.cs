﻿namespace Vbot.Enumerations
{
    public enum YoutubeVideoType
    {
        Video,
        PreStartLive,
        InProgressLive,
        FinishedLive
    }
}
