﻿using Discord;

namespace Vbot.Consts
{
    public static class EmojiConsts
    {
        public static readonly IEmote Succeed = new Emoji("\u2764");
        public static readonly IEmote Fail = new Emoji("\U0001F494");
    }
}
