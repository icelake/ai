﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using Vbot.DataStores;
using Vbot.Extensions;
using Vbot.Formatters;

namespace Vbot
{
    public class Startup : IStartup
    {
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly IConfiguration configuration;

        public Startup(IHostingEnvironment env, IConfiguration config)
        {
            hostingEnvironment = env;
            configuration = config;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            services.AddTaskLoop();
            services.AddHttpClient();
            services.AddCommonDiscord(configuration);
            services.AddDiscord(configuration);
            services.AddYoutube(configuration);
            services.AddNiconico(configuration);
            services.AddShowroom(configuration);
            services.AddTwitter(configuration);

            var dbOptions = new DbContextOptionsBuilder<DataContext>()
                .UseSqlite("DataSource=data.db")
                .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking)
                .Options;
            services.AddSingleton(dbOptions);

            services.AddMvc(config =>
            {
                config.InputFormatters.Add(new RawBodyInputFormatter());
            });

            return services.BuildServiceProvider();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseMvcWithDefaultRoute();
        }
    }
}
