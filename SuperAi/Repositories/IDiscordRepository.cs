﻿using System.Threading.Tasks;
using Vbot.DataModels;

namespace Vbot.Repositories
{
    public interface IDiscordRepository
    {
        Task<DiscordChannel> GetAsync(ulong channel);
        Task<DiscordChannel> GetIncludeListAsync(ulong channel);
        Task<DiscordChannel> RegisterAsync(ulong channel);
        Task UnregisterAsync(ulong channel);
    }
}
