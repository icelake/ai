﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vbot.DataModels;

namespace Vbot.Repositories
{
    public interface ITwitterRepository
    {
        Task<List<long>> GetUserIdsAsync();
        Task<List<TwitterUser>> GetAsync(long userId);
        Task RegisterAsync(ulong discordChannel, long userId, string pattern);
        Task UnregisterAsync(ulong discordChannel, long userId, string pattern);
    }
}
