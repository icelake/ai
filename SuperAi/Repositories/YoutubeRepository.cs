﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vbot.DataModels;
using Vbot.DataStores;

namespace Vbot.Repositories
{
    public class YoutubeRepository : IYoutubeRepository
    {
        private readonly DbContextOptions<DataContext> options;

        public YoutubeRepository(DbContextOptions<DataContext> options)
        {
            this.options = options;
        }

        public async Task<List<string>> GetChannelIdsAsync()
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                return await db.YoutubeChannels
                    .Select(b => b.Channel)
                    .ToListAsync();
            }
        }

        public async Task RegisterAsync(ulong discordChannel, string channel)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                var junction = new DiscordYoutube();

                var discordItem = await db.DiscordChannels
                    .SingleOrDefaultAsync(b => b.Channel == discordChannel);
                if (discordItem == null)
                {
                    junction.Discord = new DiscordChannel
                    {
                        Channel = discordChannel
                    };
                }
                else
                {
                    junction.DiscordId = discordItem.Id;
                }
                var item = await db.YoutubeChannels
                    .SingleOrDefaultAsync(b => b.Channel == channel);
                if (item == null)
                {
                    junction.Youtube = new YoutubeChannel
                    {
                        Channel = channel,
                        LastNotify = DateTime.UtcNow.Ticks
                    };
                }
                else
                {
                    junction.YoutubeId = item.Id;
                }
                await db.AddAsync(junction);
                await db.SaveChangesAsync(); // TODO DbUpdateConcurrencyException
                transaction.Commit();
            }
        }

        public async Task UnregisterAsync(ulong discordChannel, string channel)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                var channels = db.DiscordYoutubes
                    .Include(b => b.Youtube)
                    .Where(b => b.Youtube.Channel == channel);
                var item = await channels
                    .Include(b => b.Discord)
                    .SingleAsync(b => b.Discord.Channel == discordChannel);
                db.Remove(item);
                if (await channels.CountAsync() <= 1)
                {
                    db.Remove(item.Youtube);
                }
                await db.SaveChangesAsync(); // TODO DbUpdateConcurrencyException
                transaction.Commit();
            }
        }

        public async Task<bool> AddLiveAsync(string channelId, YoutubeVideo video)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                var channel = await db.YoutubeChannels
                    .Include(b => b.Live)
                    .SingleAsync(b => b.Channel == channelId);
                if (channel.Live != null)
                {
                    if (channel.Live.VideoId == video.VideoId && channel.Live.VideoName == video.VideoName)
                    {
                        return false;
                    }
                    video.ChannelId = channel.Id;
                    db.Update(video);
                }
                else
                {
                    video.ChannelId = channel.Id;
                    db.Add(video);
                }
                await db.SaveChangesAsync(); // TODO DbUpdateConcurrencyException
                transaction.Commit();
                return true;
            }
        }

        public async Task<List<string>> GetLiveIdsAsync()
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                return await db.YoutubeVideos
                    .Select(b => b.VideoId)
                    .ToListAsync();
            }
        }

        public async Task<YoutubeChannel> UpdateLastNotifyTimeAsync(string channelId, long published)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                var item = await db.YoutubeChannels
                    .Include(b => b.Discord)
                    .ThenInclude(b => b.Discord)
                    //.Include(b => b.Live)
                    .SingleAsync(b => b.Channel == channelId);
                if (item.LastNotify >= published)
                {
                    return null;
                }
                item.LastNotify = published;
                db.Update(item);
                await db.SaveChangesAsync(); // TODO DbUpdateConcurrencyException
                transaction.Commit();
                return item;
            }
        }

        public async Task RemoveLiveByIdAsync(string videoId)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                var item = await db.YoutubeVideos
                    .SingleAsync(b => b.VideoId == videoId);
                db.Remove(item);
                await db.SaveChangesAsync(); // TODO DbUpdateConcurrencyException
                transaction.Commit();
            }
        }

        public async Task RemoveLiveByChannelAsync(string channelId)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                var item = await db.YoutubeChannels
                    .Include(b => b.Live)
                    .SingleAsync(b => b.Channel == channelId);
                if (item.Live != null)
                {
                    db.Remove(item.Live);
                }
                await db.SaveChangesAsync(); // TODO DbUpdateConcurrencyException
                transaction.Commit();
            }
        }

        public async Task<YoutubeVideo> PopStartedLiveByIdAsync(string videoId, long published)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                var item = await db.YoutubeVideos
                    .Include(b => b.Channel)
                    .ThenInclude(b => b.Discord)
                    .ThenInclude(b => b.Discord)
                    .SingleAsync(b => b.VideoId == videoId);
                if (item.Channel.LastNotify >= published)
                {
                    return null;
                }
                item.Channel.LastNotify = published;
                db.Update(item.Channel);
                db.Remove(item);
                await db.SaveChangesAsync(); // TODO DbUpdateConcurrencyException
                transaction.Commit();
                return item;
            }
        }

        public async Task<YoutubeChannel> PopStartedLiveByChannelAsync(string channelId, long published)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                var item = await db.YoutubeChannels
                    .Include(b => b.Discord)
                    .ThenInclude(b => b.Discord)
                    .Include(b => b.Live)
                    .SingleAsync(b => b.Channel == channelId);
                if (item.LastNotify >= published)
                {
                    return null;
                }
                item.LastNotify = published;
                db.Update(item);
                if (item.Live != null)
                {
                    db.Remove(item.Live);
                }
                await db.SaveChangesAsync(); // TODO DbUpdateConcurrencyException
                transaction.Commit();
                return item;
            }
        }
    }
}
