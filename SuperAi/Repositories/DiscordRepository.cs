﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Vbot.DataModels;
using Vbot.DataStores;

namespace Vbot.Repositories
{
    public class DiscordRepository : IDiscordRepository
    {
        private readonly DbContextOptions<DataContext> options;

        public DiscordRepository(DbContextOptions<DataContext> options)
        {
            this.options = options;
        }

        public async Task<DiscordChannel> GetAsync(ulong channel)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                return await db.DiscordChannels
                    .SingleOrDefaultAsync(b => b.Channel == channel);
            }
        }

        public async Task<DiscordChannel> GetIncludeListAsync(ulong channel)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                return await db.DiscordChannels
                    .Include(b => b.Youtube).ThenInclude(b => b.Youtube)
                    .Include(b => b.Nicovideo).ThenInclude(b => b.Nicovideo)
                    .Include(b => b.Nicolive).ThenInclude(b => b.Nicolive)
                    .Include(b => b.Showroom).ThenInclude(b => b.Showroom)
                    .Include(b => b.Twitter).ThenInclude(b => b.Twitter)
                    .SingleOrDefaultAsync(b => b.Channel == channel);
            }
        }

        public async Task<DiscordChannel> RegisterAsync(ulong channel)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                var item = new DiscordChannel
                {
                    Channel = channel
                };
                await db.AddAsync(item);
                await db.SaveChangesAsync(); // TODO DbUpdateConcurrencyException
                transaction.Commit();
                return item;
            }
        }

        public async Task UnregisterAsync(ulong channel)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                var item = await db.DiscordChannels
                    .SingleAsync(b => b.Channel == channel);
                db.Remove(item);
                await db.SaveChangesAsync(); // TODO DbUpdateConcurrencyException
                transaction.Commit();
            }
        }
    }
}
