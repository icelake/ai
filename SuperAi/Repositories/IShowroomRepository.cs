﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vbot.DataModels;

namespace Vbot.Repositories
{
    public interface IShowroomRepository
    {
        Task<List<string>> GetChannelIdsAsync();
        Task RegisterAsync(ulong discordChannel, string channel);
        Task UnregisterAsync(ulong discordChannel, string channel);
        Task<ShowroomChannel> CheckAndUpdateLastNotifyTimeAsync(string channelId, long lastNotify);
    }
}
