﻿using System.Threading.Tasks;
using Vbot.DataModels;

namespace Vbot.Repositories
{
    public interface INicovideoRepository
    {
        Task<NicovideoChannel> GetAsync(string channel);
        Task RegisterAsync(ulong discordChannel, string channel);
        Task UnregisterAsync(ulong discordChannel, string channel);
    }
}
