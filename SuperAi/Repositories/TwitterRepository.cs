﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vbot.DataModels;
using Vbot.DataStores;

namespace Vbot.Repositories
{
    public class TwitterRepository : ITwitterRepository
    {
        private readonly DbContextOptions<DataContext> options;

        public TwitterRepository(DbContextOptions<DataContext> options)
        {
            this.options = options;
        }

        public async Task<List<long>> GetUserIdsAsync()
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                return await db.TwitterUsers
                    .Select(b => b.UserId)
                    .ToListAsync();
            }
        }

        public async Task<List<TwitterUser>> GetAsync(long userId)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                return await db.TwitterUsers
                    .Include(b => b.Discord)
                    .ThenInclude(b => b.Discord)
                    .Where(b => b.UserId == userId)
                    .ToListAsync();
            }
        }

        public async Task RegisterAsync(ulong discordChannel, long userId, string pattern)
        {
            if (pattern == null)
            {
                pattern = "";
            }
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                var discordItem = await db.DiscordChannels
                    .SingleOrDefaultAsync(b => b.Channel == discordChannel);
                if (discordItem == null)
                {
                    discordItem = new DiscordChannel
                    {
                        Channel = discordChannel
                    };
                }
                var item = await db.TwitterUsers
                    .SingleOrDefaultAsync(b => b.UserId == userId && b.Pattern == pattern);
                if (item == null)
                {
                    item = new TwitterUser
                    {
                        UserId = userId,
                        Pattern = pattern
                    };
                }
                await db.AddAsync(new DiscordTwitter
                {
                    Discord = discordItem,
                    Twitter = item
                });
                await db.SaveChangesAsync(); // TODO DbUpdateConcurrencyException
                transaction.Commit();
            }
        }

        public async Task UnregisterAsync(ulong discordChannel, long userId, string pattern)
        {
            if (pattern == null)
            {
                pattern = "";
            }
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                var item = await db.DiscordTwitters
                    .Include(b => b.Discord)
                    .Include(b => b.Twitter)
                    .SingleAsync(b => b.Twitter.UserId == userId && b.Twitter.Pattern == pattern && b.Discord.Channel == discordChannel);
                db.Remove(item);
                await db.SaveChangesAsync(); // TODO DbUpdateConcurrencyException
                transaction.Commit();
            }
        }
    }
}
