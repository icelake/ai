﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vbot.DataModels;

namespace Vbot.Repositories
{
    public interface IYoutubeRepository
    {
        Task<List<string>> GetChannelIdsAsync();
        Task RegisterAsync(ulong discordChannel, string channel);
        Task UnregisterAsync(ulong discordChannel, string channel);
        Task<List<string>> GetLiveIdsAsync();
        Task<YoutubeChannel> UpdateLastNotifyTimeAsync(string channelId, long published);
        Task<bool> AddLiveAsync(string channelId, YoutubeVideo video);
        Task RemoveLiveByIdAsync(string videoId);
        Task RemoveLiveByChannelAsync(string channelId);
        Task<YoutubeVideo> PopStartedLiveByIdAsync(string videoId, long published);
        Task<YoutubeChannel> PopStartedLiveByChannelAsync(string channelId, long published);
    }
}
