﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Vbot.DataModels;
using Vbot.DataStores;

namespace Vbot.Repositories
{
    public class NicovideoRepository : INicovideoRepository
    {
        private readonly DbContextOptions<DataContext> options;

        public NicovideoRepository(DbContextOptions<DataContext> options)
        {
            this.options = options;
        }

        public async Task<NicovideoChannel> GetAsync(string channel)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                return await db.NicovideoChannels
                    .Include(b => b.Discord)
                    .ThenInclude(b => b.Discord)
                    .SingleOrDefaultAsync(b => b.Channel == channel);
            }
        }

        public async Task RegisterAsync(ulong discordChannel, string channel)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                var discordItem = await db.DiscordChannels
                    .SingleOrDefaultAsync(b => b.Channel == discordChannel);
                if (discordItem == null)
                {
                    discordItem = new DiscordChannel
                    {
                        Channel = discordChannel
                    };
                }
                var item = await db.NicovideoChannels
                    .SingleOrDefaultAsync(b => b.Channel == channel);
                if (item == null)
                {
                    item = new NicovideoChannel
                    {
                        Channel = channel
                    };
                }
                await db.AddAsync(new DiscordNicovideo
                {
                    Discord = discordItem,
                    Nicovideo = item
                });
                await db.SaveChangesAsync(); // TODO DbUpdateConcurrencyException
                transaction.Commit();
            }
        }

        public async Task UnregisterAsync(ulong discordChannel, string channel)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                var item = await db.DiscordNicovideos
                    .Include(b => b.Discord)
                    .Include(b => b.Nicovideo)
                    .SingleAsync(b => b.Nicovideo.Channel == channel && b.Discord.Channel == discordChannel);
                db.Remove(item);
                await db.SaveChangesAsync(); // TODO DbUpdateConcurrencyException
                transaction.Commit();
            }
        }
    }
}
