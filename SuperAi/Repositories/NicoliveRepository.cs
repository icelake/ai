﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Vbot.DataModels;
using Vbot.DataStores;

namespace Vbot.Repositories
{
    public class NicoliveRepository : INicoliveRepository
    {
        private readonly DbContextOptions<DataContext> options;

        public NicoliveRepository(DbContextOptions<DataContext> options)
        {
            this.options = options;
        }
        
        public async Task<NicoliveChannel> GetAsync(string channel)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                return await db.NicoliveChannels
                    .Include(b => b.Discord)
                    .ThenInclude(b => b.Discord)
                    .SingleOrDefaultAsync(b => b.Channel == channel);
            }
        }

        public async Task RegisterAsync(ulong discordChannel, string channel)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                var discordItem = await db.DiscordChannels
                    .SingleOrDefaultAsync(b => b.Channel == discordChannel);
                if (discordItem == null)
                {
                    discordItem = new DiscordChannel
                    {
                        Channel = discordChannel
                    };
                }
                var item = await db.NicoliveChannels
                    .SingleOrDefaultAsync(b => b.Channel == channel);
                if (item == null)
                {
                    item = new NicoliveChannel
                    {
                        Channel = channel
                    };
                }
                await db.AddAsync(new DiscordNicolive
                {
                    Discord = discordItem,
                    Nicolive = item
                });
                await db.SaveChangesAsync(); // TODO DbUpdateConcurrencyException
                transaction.Commit();
            }
        }

        public async Task UnregisterAsync(ulong discordChannel, string channel)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                var item = await db.DiscordNicolives
                    .Include(b => b.Discord)
                    .Include(b => b.Nicolive)
                    .SingleAsync(b => b.Nicolive.Channel == channel && b.Discord.Channel == discordChannel);
                db.Remove(item);
                await db.SaveChangesAsync(); // TODO DbUpdateConcurrencyException
                transaction.Commit();
            }
        }
    }
}
