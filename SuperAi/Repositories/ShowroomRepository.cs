﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vbot.DataModels;
using Vbot.DataStores;

namespace Vbot.Repositories
{
    public class ShowroomRepository : IShowroomRepository
    {
        private readonly DbContextOptions<DataContext> options;

        public ShowroomRepository(DbContextOptions<DataContext> options)
        {
            this.options = options;
        }

        public async Task<List<string>> GetChannelIdsAsync()
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                return await db.ShowroomChannels
                    .Select(b => b.Channel)
                    .ToListAsync();
            }
        }

        public async Task RegisterAsync(ulong discordChannel, string channel)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                var discordItem = await db.DiscordChannels
                    .SingleOrDefaultAsync(b => b.Channel == discordChannel);
                if (discordItem == null)
                {
                    discordItem = new DiscordChannel
                    {
                        Channel = discordChannel
                    };
                }
                var item = await db.ShowroomChannels
                    .SingleOrDefaultAsync(b => b.Channel == channel);
                if (item == null)
                {
                    item = new ShowroomChannel
                    {
                        Channel = channel
                    };
                }
                await db.AddAsync(new DiscordShowroom
                {
                    Discord = discordItem,
                    Showroom = item
                });
                await db.SaveChangesAsync(); // TODO DbUpdateConcurrencyException
                transaction.Commit();
            }
        }

        public async Task UnregisterAsync(ulong discordChannel, string channel)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                var item = await db.DiscordShowrooms
                    .Include(b => b.Discord)
                    .Include(b => b.Showroom)
                    .SingleAsync(b => b.Showroom.Channel == channel && b.Discord.Channel == discordChannel);
                db.Remove(item);
                await db.SaveChangesAsync(); // TODO DbUpdateConcurrencyException
                transaction.Commit();
            }
        }

        public async Task<ShowroomChannel> CheckAndUpdateLastNotifyTimeAsync(string channelId, long lastNotify)
        {
            using (var db = new DataContext(options))
            using (var transaction = await db.Database.BeginTransactionAsync())
            {
                var item = await db.ShowroomChannels
                    .Include(b => b.Discord)
                    .ThenInclude(b => b.Discord)
                    .SingleAsync(b => b.Channel == channelId);
                if (item.LastNotify >= lastNotify)
                {
                    return null;
                }
                item.LastNotify = lastNotify;
                db.Update(item);
                await db.SaveChangesAsync(); // TODO DbUpdateConcurrencyException
                transaction.Commit();
                return item;
            }
        }
    }
}
