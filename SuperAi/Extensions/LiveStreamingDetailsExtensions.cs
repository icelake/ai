﻿using Google.Apis.YouTube.v3.Data;
using Vbot.Enumerations;

namespace Vbot.Extensions
{
    public static class LiveStreamingDetailsExtensions
    {
        public static YoutubeVideoType GetVideoType(this VideoLiveStreamingDetails live)
        {
            if (live == null)
                return YoutubeVideoType.Video;
            if (!live.ActualStartTime.HasValue)
                return YoutubeVideoType.PreStartLive;
            else if (!live.ActualEndTime.HasValue)
                return YoutubeVideoType.InProgressLive;
            else
                return YoutubeVideoType.FinishedLive;
        }
    }
}
