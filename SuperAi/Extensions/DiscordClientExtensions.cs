﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vbot.Clients;
using Vbot.DataModels;
using Vbot.Models;

namespace Vbot.Extensions
{
    public static class DiscordClientExtensions
    {
        public static Task PostFeedAsync<T>(this IDiscordMessageClient discordClient, IEnumerable<T> channels, IFeed feed) where T : IDiscordJunction
        {
            return discordClient.PostFeedAsync(channels.Select(b => b.Discord.Channel), feed);
        }

        public static Task PostFeedAsync(this IDiscordMessageClient discordClient, IEnumerable<ulong> channels, IFeed feed)
        {
            foreach (var channel in channels)
            {
                if (!feed.IsLive)
                {
                    discordClient.SendMessageQueue(channel, $":arrow_forward: 【新着動画】**{feed.Title}**\n{feed.Url}");
                }
                else
                {
                    discordClient.SendMessageQueue(channel, $":cinema: 【ライブ配信】**{feed.Title}**\n{feed.Url}");
                }
            }
            return Task.CompletedTask;
        }

        public static Task PostStringAsync<T>(this IDiscordMessageClient discordClient, IEnumerable<T> channels, string text) where T : IDiscordJunction
        {
            return discordClient.PostStringAsync(channels.Select(b => b.Discord.Channel), text);
        }

        public static Task PostStringAsync(this IDiscordMessageClient discordClient, IEnumerable<ulong> channels, string text)
        {
            foreach (var channel in channels)
            {
                discordClient.SendMessageQueue(channel, text);
            }
            return Task.CompletedTask;
        }
    }
}
