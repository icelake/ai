﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Vbot.AppOptions;
using Vbot.Clients;
using Vbot.Repositories;
using Vbot.Services;
using Vbot.UseCases.Controllers;
using Vbot.UseCases.Modules;
using Vbot.UseCases.Services;

namespace Vbot.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddDiscord(this IServiceCollection services, IConfiguration configuration)
        {
            // configure
            //
            // frameworks
            //
            // clients
            //
            // client factories
            //
            // services
            services.AddHostedService<DiscordMessageService>();
            // repositories
            services.AddSingleton<IDiscordRepository, DiscordRepository>();
            // usecases
            services.AddSingleton<IChannelModuleUseCase, ChannelModuleUseCase>();
            services.AddSingleton<IDiscordMessageServiceUseCase, DiscordMessageServiceUseCase>();
        }

        public static void AddYoutube(this IServiceCollection services, IConfiguration configuration)
        {
            // configure
            services.Configure<ServerOptions>(configuration.GetSection("Server"));
            services.Configure<GoogleOptions>(configuration.GetSection("Google"));
            services.Configure<PshbOptions>(configuration.GetSection("Pshb"));
            // frameworks
            //
            // clients
            services.AddTransient<IYoutubeApiClient, YoutubeApiClient>();
            services.AddTransient<IYoutubeHttpClient, YoutubeHttpClient>();
            services.AddTransient<IYoutubeSubscribeClient, YoutubeSubscribeClient>();
            // client factories
            services.AddTransient<IYoutubeApiClientFactory, YoutubeApiClientFactory>();
            services.AddTransient<IYoutubeHttpClientFactory, YoutubeHttpClientFactory>();
            services.AddTransient<IYoutubeSubscribeClientFactory, YoutubeSubscribeClientFactory>();
            // services
            services.AddHostedService<YoutubeService>();
            // repositories
            services.AddSingleton<IYoutubeRepository, YoutubeRepository>();
            // usecases
            services.AddSingleton<IYoutubeServiceUseCase, YoutubeServiceUseCase>();
            services.AddSingleton<IYoutubeModuleUseCase, YoutubeModuleUseCase>();
            services.AddSingleton<IYoutubeControllerUseCase, YoutubeControllerUseCase>();
        }

        public static void AddNiconico(this IServiceCollection services, IConfiguration configuration)
        {
            // configure
            services.Configure<NiconicoOptions>(configuration.GetSection("Niconico"));
            // frameworks
            //
            // clients
            services.AddTransient<INiconicoClient, NiconicoClient>();
            // client factories
            services.AddTransient<INiconicoClientFactory, NiconicoClientFactory>();
            // services
            services.AddHostedService<NiconicoService>();
            // repositories
            services.AddSingleton<INicoliveRepository, NicoliveRepository>();
            services.AddSingleton<INicovideoRepository, NicovideoRepository>();
            // usecases
            services.AddSingleton<INiconicoServiceUseCase, NiconicoServiceUseCase>();
            services.AddSingleton<INicoliveModuleUseCase, NicoliveModuleUseCase>();
            services.AddSingleton<INicovideoModuleUseCase, NicovideoModuleUseCase>();
        }

        public static void AddShowroom(this IServiceCollection services, IConfiguration configuration)
        {
            // configure
            //
            // frameworks
            //
            // clients
            services.AddTransient<IShowroomClient, ShowroomClient>();
            // client factories
            services.AddTransient<IShowroomClientFactory, ShowroomClientFactory>();
            // services
            services.AddHostedService<ShowroomService>();
            // repositories
            services.AddSingleton<IShowroomRepository, ShowroomRepository>();
            // usecases
            services.AddSingleton<IShowroomServiceUseCase, ShowroomServiceUseCase>();
            services.AddSingleton<IShowroomModuleUseCase, ShowroomModuleUseCase>();
        }

        public static void AddTwitter(this IServiceCollection services, IConfiguration configuration)
        {
            // configure
            services.Configure<TwitterOptions>(configuration.GetSection("Twitter"));
            // frameworks
            //
            // clients
            services.AddTransient<ITwitterClient, TwitterClient>();
            // client factories
            services.AddTransient<ITwitterClientFactory, TwitterClientFactory>();
            // services
            services.AddHostedService<TwitterService>();
            // repositories
            services.AddSingleton<ITwitterRepository, TwitterRepository>();
            // usecases
            services.AddSingleton<ITwitterServiceUseCase, TwitterServiceUseCase>();
            services.AddSingleton<ITwitterModuleUseCase, TwitterModuleUseCase>();
        }
    }
}
