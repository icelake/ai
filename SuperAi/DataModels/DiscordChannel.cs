﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Vbot.DataModels
{
    public class DiscordChannel
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public ulong Channel { get; set; }

        //[ConcurrencyCheck]
        //[Timestamp]
        //public byte[] TimeStamp { get; set; }

        public ICollection<DiscordYoutube> Youtube { get; set; }
        public ICollection<DiscordNicolive> Nicolive { get; set; }
        public ICollection<DiscordNicovideo> Nicovideo { get; set; }
        public ICollection<DiscordShowroom> Showroom { get; set; }
        public ICollection<DiscordTwitter> Twitter { get; set; }
    }
}
