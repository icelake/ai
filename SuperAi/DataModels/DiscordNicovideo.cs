﻿using System.ComponentModel.DataAnnotations;

namespace Vbot.DataModels
{
    public class DiscordNicovideo : IDiscordJunction
    {
        public int DiscordId { get; set; }
        public DiscordChannel Discord { get; set; }

        public int NicovideoId { get; set; }
        public NicovideoChannel Nicovideo { get; set; }

        //[ConcurrencyCheck]
        //[Timestamp]
        //public byte[] TimeStamp { get; set; }

        public override string ToString()
        {
            return Nicovideo.ToString();
        }
    }
}
