﻿using System.ComponentModel.DataAnnotations;

namespace Vbot.DataModels
{
    public class DiscordShowroom : IDiscordJunction
    {
        public int DiscordId { get; set; }
        public DiscordChannel Discord { get; set; }

        public int ShowroomId { get; set; }
        public ShowroomChannel Showroom { get; set; }

        //[ConcurrencyCheck]
        //[Timestamp]
        //public byte[] TimeStamp { get; set; }

        public override string ToString()
        {
            return Showroom.ToString();
        }
    }
}
