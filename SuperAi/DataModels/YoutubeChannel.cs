﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Vbot.DataModels
{
    public class YoutubeChannel
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(32)]
        public string Channel { get; set; }
        [Required]
        public long LastNotify { get; set; }

        //[ConcurrencyCheck]
        //[Timestamp]
        //public byte[] TimeStamp { get; set; }

        public ICollection<DiscordYoutube> Discord { get; set; }
        public YoutubeVideo Live { get; set; }

        public override string ToString()
        {
            return $"https://www.youtube.com/channel/{Channel}";
        }
    }
}
