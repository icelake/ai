﻿namespace Vbot.DataModels
{
    public interface IDiscordJunction
    {
        int DiscordId { get; set; }
        DiscordChannel Discord { get; set; }
    }
}
