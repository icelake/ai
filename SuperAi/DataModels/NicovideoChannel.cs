﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Vbot.DataModels
{
    public class NicovideoChannel
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Channel { get; set; }

        //[ConcurrencyCheck]
        //[Timestamp]
        //public byte[] TimeStamp { get; set; }

        public ICollection<DiscordNicovideo> Discord { get; set; }

        public override string ToString()
        {
            return $"http://www.nicovideo.jp/user/{Channel}";
        }
    }
}
