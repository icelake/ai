﻿using System.ComponentModel.DataAnnotations;

namespace Vbot.DataModels
{
    public class DiscordNicolive : IDiscordJunction
    {
        public int DiscordId { get; set; }
        public DiscordChannel Discord { get; set; }

        public int NicoliveId { get; set; }
        public NicoliveChannel Nicolive { get; set; }

        //[ConcurrencyCheck]
        //[Timestamp]
        //public byte[] TimeStamp { get; set; }

        public override string ToString()
        {
            return Nicolive.ToString();
        }
    }
}
