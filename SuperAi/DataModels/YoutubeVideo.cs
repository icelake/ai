﻿using System.ComponentModel.DataAnnotations;

namespace Vbot.DataModels
{
    public class YoutubeVideo
    {
        [Key]
        public int ChannelId { get; set; }
        [Required]
        [StringLength(32)]
        public string VideoId { get; set; }
        [Required]
        [StringLength(100)]
        public string VideoName { get; set; }

        //[ConcurrencyCheck]
        //[Timestamp]
        //public byte[] TimeStamp { get; set; }

        public YoutubeChannel Channel { get; set; }
    }
}
