﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Vbot.DataModels
{
    public class TwitterUser
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public long UserId { get; set; }
        [Required]
        public string Pattern { get; set; }

        //[ConcurrencyCheck]
        //[Timestamp]
        //public byte[] TimeStamp { get; set; }

        public ICollection<DiscordTwitter> Discord { get; set; }

        public override string ToString()
        {
            return $"https://twitter.com/intent/user?user_id={UserId}";
        }
    }
}
