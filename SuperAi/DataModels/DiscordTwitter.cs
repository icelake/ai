﻿using System.ComponentModel.DataAnnotations;

namespace Vbot.DataModels
{
    public class DiscordTwitter : IDiscordJunction
    {
        public int DiscordId { get; set; }
        public DiscordChannel Discord { get; set; }

        public int TwitterId { get; set; }
        public TwitterUser Twitter { get; set; }

        //[ConcurrencyCheck]
        //[Timestamp]
        //public byte[] TimeStamp { get; set; }

        public override string ToString()
        {
            return Twitter.ToString();
        }
    }
}
