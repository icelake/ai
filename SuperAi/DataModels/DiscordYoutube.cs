﻿using System.ComponentModel.DataAnnotations;

namespace Vbot.DataModels
{
    public class DiscordYoutube : IDiscordJunction
    {
        public int DiscordId { get; set; }
        public DiscordChannel Discord { get; set; }

        public int YoutubeId { get; set; }
        public YoutubeChannel Youtube { get; set; }

        //[ConcurrencyCheck]
        //[Timestamp]
        //public byte[] TimeStamp { get; set; }

        public override string ToString()
        {
            return Youtube.ToString();
        }
    }
}
