﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Vbot.DataModels
{
    public class ShowroomChannel
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(128)]
        public string Channel { get; set; }
        [Required]
        public long LastNotify { get; set; }

        //[ConcurrencyCheck]
        //[Timestamp]
        //public byte[] TimeStamp { get; set; }

        public ICollection<DiscordShowroom> Discord { get; set; }

        public override string ToString()
        {
            return $"https://showroom-live.com/{Channel}";
        }
    }
}
