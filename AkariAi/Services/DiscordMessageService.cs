﻿using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;
using Vbot.UseCases.Services;

namespace Vbot.Services
{
    public class DiscordMessageService : BackgroundService
    {
        private readonly IDiscordMessageServiceUseCase useCase;

        public DiscordMessageService(IDiscordMessageServiceUseCase discordMessageServiceUseCase)
        {
            this.useCase = discordMessageServiceUseCase;
        }

        public override async Task StartAsync(CancellationToken cancellationToken)
        {
            await useCase.StartAsync(cancellationToken);
            await base.StartAsync(cancellationToken);
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            await useCase.StopAsync(cancellationToken);
            await base.StopAsync(cancellationToken);
        }

        protected override Task ExecuteAsync(CancellationToken cancellationToken)
        {
            return useCase.ExecuteAsync(cancellationToken);
        }
    }
}
