﻿using Discord;
using System.Collections.Generic;
using System.Linq;

namespace Vbot.Extensions
{
    public static class DiscordRoleExtensions
    {
        public static T Get<T>(this IEnumerable<T> roles, string name) where T : IRole
        {
            return roles
                .Where(r => r.Name == name)
                .SingleOrDefault();
        }
    }
}
