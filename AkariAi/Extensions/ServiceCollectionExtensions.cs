﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Vbot.AppOptions;
using Vbot.Clients;
using Vbot.Services;
using Vbot.UseCases.Services;

namespace Vbot.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddDiscord(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHostedService<DiscordMessageService>();
            services.AddSingleton<IDiscordMessageServiceUseCase, DiscordMessageServiceUseCase>();
        }

        public static void AddTalkClient(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<UserlocalOptions>(configuration.GetSection("Userlocal"));
            services.AddTransient<ITalkClient, UserlocalTalkClient>();
        }
    }
}
