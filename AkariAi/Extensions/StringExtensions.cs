﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vbot.Extensions
{
    public static class StringExtensions
    {
        private static readonly char[] RemoveCharactorList =
        {
            ' ', '　', '"', '#', '\\', '$', '%', '&', '-', '^', '\'', '@', ';', ':', '.', '/', '=', '~', '|', '`', '+', '*', '_', '＂', '＃', '＄', '％', '＆', '＇', '－', '＾', '＼', '＠', '；', '：', '，', '．', '￥', '／', '＝', '～', '｜', '｀', '＋', '＊', '＿', '・', '\r', '\n', '\t'
        };
        private static readonly Dictionary<char, char> KatakanaMap = new Dictionary<char, char>
        {
            { 'ｦ', 'を' },{ 'ｧ', 'ぁ' },{ 'ｨ', 'ぃ' },{ 'ｩ', 'ぅ' },{ 'ｪ', 'ぇ' },{ 'ｫ', 'ぉ' },{ 'ｬ', 'ゃ' },{ 'ｭ', 'ゅ' },{ 'ｮ', 'ょ' },{ 'ｯ', 'っ' },{ 'ｱ', 'あ' },{ 'ｲ', 'い' },{ 'ｳ', 'う' },{ 'ｴ', 'え' },{ 'ｵ', 'お' },{ 'ｶ', 'か' },{ 'ｷ', 'き' },{ 'ｸ', 'く' },{ 'ｹ', 'け' },{ 'ｺ', 'こ' },{ 'ｻ', 'さ' },{ 'ｼ', 'し' },{ 'ｽ', 'す' },{ 'ｾ', 'せ' },{ 'ｿ', 'そ' },{ 'ﾀ', 'た' },{ 'ﾁ', 'ち' },{ 'ﾂ', 'つ' },{ 'ﾃ', 'て' },{ 'ﾄ', 'と' },{ 'ﾅ', 'な' },{ 'ﾆ', 'に' },{ 'ﾇ', 'ぬ' },{ 'ﾈ', 'ね' },{ 'ﾉ', 'の' },{ 'ﾊ', 'は' },{ 'ﾋ', 'ひ' },{ 'ﾌ', 'ふ' },{ 'ﾍ', 'へ' },{ 'ﾎ', 'ほ' },{ 'ﾏ', 'ま' },{ 'ﾐ', 'み' },{ 'ﾑ', 'む' },{ 'ﾒ', 'め' },{ 'ﾓ', 'も' },{ 'ﾔ', 'や' },{ 'ﾕ', 'ゆ' },{ 'ﾖ', 'よ' },{ 'ﾗ', 'ら' },{ 'ﾘ', 'り' },{ 'ﾙ', 'る' },{ 'ﾚ', 'れ' },{ 'ﾛ', 'ろ' },{ 'ﾜ', 'わ' },{ 'ﾝ', 'ん' }
        };

        public static string SanitizeMessage(this string input)
        {
            var length = input.Length;
            var sb = new StringBuilder(length);
            for (int i = 0; i < length; i++)
            {
                var c = input[i];
                if (RemoveCharactorList.Contains(c))
                {
                    continue;
                }
                if (c == '？')
                {
                    c = '?';
                }
                else if ('\uFF21' <= c && c <= '\uFF3A') // full uppercase
                {
                    c = (char)(c - ('\uFF21' - '\u0040')); // half uppercase
                }
                else if ('\uFF41' <= c && c <= '\uFF5A') // full lowercase
                {
                    c = (char)(c - ('\uFF41' - '\u0040')); // half uppercase
                }
                else if ('\u0061' <= c && c <= '\u007A') // half lowercase
                {
                    c = (char)(c - ('\u0061' - '\u0040')); // half uppercase
                }
                else if ('\u30A1' <= c && c <= '\u30F6') // full katakana
                {
                    c = (char)(c - ('\u30A1' - '\u3041')); // full hiragana
                }
                else if ('\uFF66' <= c && c <= '\uFF9D') // full katakana
                {
                    c = KatakanaMap[c]; // full hiragana
                }
                sb.Append(c);
            }
            return sb.ToString();
        }
    }
}
