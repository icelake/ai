﻿using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Vbot.AppOptions;
using Vbot.Utils;

namespace Vbot.Clients
{
    public class UserlocalTalkClient : ITalkClient
    {
        private readonly string url;
        private readonly HttpClient httpClient;

        public UserlocalTalkClient(
            IOptions<UserlocalOptions> userlocalOptions,
            IHttpClientFactory httpClientFactory)
        {
            url = $"https://chatbot-api.userlocal.jp/api/chat?bot_name=%E3%82%A2%E3%82%AB%E3%83%AA&key={userlocalOptions.Value.ApiKey}";
            httpClient = httpClientFactory.CreateClient();
        }

        public async Task<string> TalkAsync(string message, string name)
        {
            var arg = new RequestArgument
            {
                Message = message,
                UserName = name
            };
            var response = await httpClient.PostAsync(url, new StringContent(JsonSerializer.Serialize(arg), Encoding.UTF8, "application/json"));
            using (var stream = await response.Content.ReadAsStreamAsync())
            {
                var res = JsonSerializer.Deserialize<ResponseArgument>(stream);
                if (res.Status == "success")
                    return res.Result;
                else
                    throw new ArgumentException(res.Status);
            }
        }

        [DataContract]
        private class RequestArgument
        {
            [DataMember(Name = "message")]
            public string Message { get; set; }

            [DataMember(Name = "user_name")]
            public string UserName { get; set; }
        }

        [DataContract]
        private class ResponseArgument
        {
            [DataMember(Name = "status")]
            public string Status { get; set; }

            [DataMember(Name = "result")]
            public string Result { get; set; }
        }
    }
}
