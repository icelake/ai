﻿using System.Threading.Tasks;

namespace Vbot.Clients
{
    public interface ITalkClient
    {
        Task<string> TalkAsync(string message, string name);
    }
}
