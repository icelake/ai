﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Vbot.AppOptions;
using Vbot.Extensions;
using Vbot.Models;
using Vbot.UseCases.Modules;

namespace Vbot
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildHost(args);
            host.Run();
        }

        public static IHost BuildHost(string[] args)
        {
            return new HostBuilder()
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                          .AddJsonFile($"appsettings.{hostingContext.HostingEnvironment.EnvironmentName}.json", optional: true, reloadOnChange: true);
                })
                .ConfigureLogging((hostingContext, logging) =>
                {
                    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    logging.SetMinimumLevel(LogLevel.Warning);
                    logging.AddConsole();
                    logging.AddDebug();
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddOptions();
                    var configuration = hostContext.Configuration;
                    services.Configure<PathOptions>(configuration.GetSection("Path"));

                    services.AddHttpClient();
                    services.AddCommonDiscord(configuration);
                    services.AddWebhookDiscord(configuration);
                    services.AddDiscord(configuration);
                    services.AddTalkClient(configuration);

                    services.AddSingleton<DiscordTalkState>();
                    services.AddSingleton<IToiletModuleUseCase, ToiletModuleUseCase>();
                    services.AddSingleton<IPlayModuleUseCase, PlayModuleUseCase>();
                    services.AddSingleton<IMusicModuleUseCase, MusicModuleUseCase>();
                })
                .Build();
        }
    }
}
