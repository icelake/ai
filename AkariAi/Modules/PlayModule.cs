﻿using Discord.Commands;
using System.Threading.Tasks;
using Vbot.Consts;
using Vbot.UseCases.Modules;

namespace Vbot.Modules
{
    [Group("play")]
    public class PlayModule : ModuleBase<SocketCommandContext>
    {
        private readonly IPlayModuleUseCase playModuleUseCase;

        public PlayModule(IPlayModuleUseCase playModuleUseCase)
        {
            this.playModuleUseCase = playModuleUseCase;
        }

        [Command("music")]
        [RequireContext(ContextType.Guild)]
        public async Task Music(string path = null)
        {
            try
            {
                playModuleUseCase.PlayMusic(Context, path);
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
            await Context.Message.AddReactionAsync(EmojiConsts.Rocket);
        }

        [Command("fortune")]
        public async Task Fortune()
        {
            try
            {
                var path = playModuleUseCase.PlayFortune(Context);
                await Context.Channel.SendFileAsync(path, $"<@{Context.User.Id}>");
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
        }

        [Command("dance", RunMode = RunMode.Async)]
        [RequireContext(ContextType.Guild)]
        public async Task Dance()
        {
            try
            {
                await playModuleUseCase.PlayDanceAsync(Context);
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
        }
    }
}
