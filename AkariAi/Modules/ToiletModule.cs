﻿using Discord.Commands;
using System.Threading.Tasks;
using Vbot.Consts;
using Vbot.UseCases.Modules;

namespace Vbot.Modules
{
    public class ToiletModule : ModuleBase<SocketCommandContext>
    {
        private readonly IToiletModuleUseCase toiletModuleUseCase;

        public ToiletModule(IToiletModuleUseCase toiletModuleUseCase)
        {
            this.toiletModuleUseCase = toiletModuleUseCase;
        }

        [Command("flush toilet")]
        [RequireContext(ContextType.Guild)]
        public async Task FlushToilet()
        {
            try
            {
                await toiletModuleUseCase.FlushToiletAsync(Context);
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
            await ReplyAsync("自分のケツは自分で拭いてくださいね");
        }

        [Command("explode toilet")]
        [RequireContext(ContextType.Guild)]
        public async Task ExplodeToilet()
        {
            try
            {
                await toiletModuleUseCase.ExplodeToiletAsync(Context);
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
            await Context.Message.AddReactionAsync(EmojiConsts.Poo);
        }
    }
}
