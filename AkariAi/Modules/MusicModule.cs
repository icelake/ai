﻿using Discord.Commands;
using System.Threading.Tasks;
using Vbot.Consts;
using Vbot.UseCases.Modules;

namespace Vbot.Modules
{
    [Group("music")]
    public class MusicModule : ModuleBase<SocketCommandContext>
    {
        private readonly IMusicModuleUseCase musicModuleUseCase;

        public MusicModule(IMusicModuleUseCase musicModuleUseCase)
        {
            this.musicModuleUseCase = musicModuleUseCase;
        }

        [Command("connect")]
        [RequireOwner]
        public async Task Connect(ulong channelId = 0)
        {
            try
            {
                musicModuleUseCase.ConnectVoiceMeeter(Context, channelId);
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
        }

        [Command("skip")]
        public async Task Skip()
        {
            try
            {
                musicModuleUseCase.Skip(Context);
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
        }

        [Command("play")]
        public async Task Play(string url, ulong channelId = 0)
        {
            try
            {
                musicModuleUseCase.Play(Context, url, channelId);
            }
            catch
            {
                await Context.Message.AddReactionAsync(EmojiConsts.Fail);
                throw;
            }
        }
    }
}
