﻿using Discord;
using Discord.Commands;
using System.Threading.Tasks;

namespace Vbot.Modules
{
    public class HelpModule : ModuleBase<SocketCommandContext>
    {
        [Command("help")]
        public async Task Help()
        {
            var builder = new EmbedBuilder()
            {
                Color = new Color(200, 228, 255),
                Title = "アカリのコマンド一覧"
            };
            builder.AddField("!hello play music", "音楽を再生する");
            builder.AddField("!hello play dance", "ダンスを踊る");
            builder.AddField("!hello play fortune", "占いをする");
            builder.AddField("!hello flush toilet", "トイレを流す");
            builder.AddField("!hello explode toilet", "トイレを爆発させる");
            await ReplyAsync("ハロー、ミライアカリだよっ:heartbeat:", embed: builder.Build());
        }
    }
}
