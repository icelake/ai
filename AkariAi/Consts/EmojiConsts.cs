﻿using Discord;

namespace Vbot.Consts
{
    public static class EmojiConsts
    {
        public static readonly IEmote Fail = new Emoji("\u274C");
        public static readonly IEmote Poo = new Emoji("\U0001F4A9");
        public static readonly IEmote Akari = Emote.Parse("<:ma1:409025750167977984>");
        public static readonly IEmote Org = Emote.Parse("<:org:409238877040017409>");
        public static readonly IEmote Rocket = new Emoji("\U0001F680");
    }
}
