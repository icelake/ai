﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord.Commands;

namespace Vbot.UseCases.Modules
{
    public interface IPlayModuleUseCase
    {
        void PlayMusic(SocketCommandContext context, string path);
        string PlayFortune(SocketCommandContext context);
        Task PlayDanceAsync(SocketCommandContext context);
    }
}
