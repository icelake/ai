﻿using Discord.Commands;

namespace Vbot.UseCases.Modules
{
    public interface IMusicModuleUseCase
    {
        void Play(SocketCommandContext context, string url, ulong channelId);
        void ConnectVoiceMeeter(SocketCommandContext context, ulong channelId);
        void Skip(SocketCommandContext context);
    }
}
