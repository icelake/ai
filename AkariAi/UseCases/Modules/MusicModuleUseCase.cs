﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Discord.Commands;
using Discord.WebSocket;
using Vbot.Clients;
using Vbot.Extensions;

namespace Vbot.UseCases.Modules
{
    public class MusicModuleUseCase : IMusicModuleUseCase
    {
        private static readonly Regex UrlPattern = new Regex(@"https?://[\w/:%#\$&\?\(\)~\.=\+\-]+");

        private readonly IDiscordAudioClient discordAudioClient;

        public MusicModuleUseCase(IDiscordAudioClient discordAudioClient)
        {
            this.discordAudioClient = discordAudioClient;
        }

        public void Play(SocketCommandContext context, string url, ulong channelId)
        {
            if (!UrlPattern.IsMatch(url))
            {
                throw new ArgumentException($"Wrong url {url}");
            }
            var channel = (channelId != 0) ? context.Client.GetChannel(channelId) as SocketVoiceChannel : context.Guild.VoiceChannels.MaxUserJoined();
            if (channel == null)
            {
                throw new ArgumentException($"Wrong channel {channel}");
            }
            discordAudioClient.EnqueueUrl(channel, url);
        }

        public void ConnectVoiceMeeter(SocketCommandContext context, ulong channelId)
        {
            var channel = (channelId != 0) ? context.Client.GetChannel(channelId) as SocketVoiceChannel : context.Guild.VoiceChannels.MaxUserJoined();
            if (channel == null)
            {
                throw new ArgumentException($"Wrong channel {channel}");
            }
            discordAudioClient.EnqueueStream(channel);
        }

        public void Skip(SocketCommandContext context)
        {
            discordAudioClient.Next();
        }
    }
}
