﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Linq;
using System.Threading.Tasks;
using Vbot.Clients;
using Vbot.Extensions;

namespace Vbot.UseCases.Modules
{
    public class ToiletModuleUseCase : IToiletModuleUseCase
    {
        private readonly IDiscordMessageClient discordClient;

        public ToiletModuleUseCase(IDiscordMessageClient discordClient)
        {
            this.discordClient = discordClient;
        }

        public async Task FlushToiletAsync(SocketCommandContext context)
        {
            var role = context.Guild.Roles.Get("うんこ");
            if (role == null)
            {
                return;
            }
            foreach (var user in context.Guild.Users)
            {
                if (!user.Roles.Any(r => r.Name.Contains("うんこ")))
                    continue;
                if (user == context.User)// 本人は除外
                    continue;
                //if ((context.User.Id == 409055434301898752 && user.Id == 397936758047768576) || (context.User.Id == 397936758047768576 && user.Id == 409055434301898752))//白黒と紅白は除外
                //    continue;
                if (user.Id == 315234496683180033)//s6は除外
                    continue;
                await discordClient.RemoveRoleAsync(user, role);
            }
            return;
        }

        public async Task ExplodeToiletAsync(SocketCommandContext context)
        {
            var role = context.Guild.Roles.Get("うんこ");
            if (role == null)
            {
                return;
            }
            var messages = await context.Channel.GetMessagesAsync(30).Flatten();
            foreach (var m in messages)
            {
                var message = m as IUserMessage;
                if (message == null)
                    continue;
                var user = message.Author as SocketGuildUser;
                if (user == null)
                    continue;
                if (user.Roles.Contains(role))
                    continue;
                await discordClient.AddRoleAsync(user, role);
            }
        }
    }
}
