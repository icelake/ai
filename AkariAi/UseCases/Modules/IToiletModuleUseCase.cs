﻿using Discord.Commands;
using System.Threading.Tasks;

namespace Vbot.UseCases.Modules
{
    public interface IToiletModuleUseCase
    {
        Task FlushToiletAsync(SocketCommandContext context);
        Task ExplodeToiletAsync(SocketCommandContext context);
    }
}
