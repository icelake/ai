﻿using Discord.Commands;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Threading.Tasks;
using Vbot.AppOptions;
using Vbot.Clients;
using Vbot.Extensions;
using Vbot.Models;

namespace Vbot.UseCases.Modules
{
    public class PlayModuleUseCase : IPlayModuleUseCase
    {
        private readonly PathOptions pathOptions;
        private readonly IDiscordAudioClient discordAudioClient;
        private readonly IDiscordWebhookClient discordWebhookClient;
        private readonly DiscordTalkState discordTalkState;

        public PlayModuleUseCase(
            IOptions<PathOptions> pathOptions,
            IDiscordAudioClient discordAudioClient,
            IDiscordWebhookClient discordWebhookClient,
            DiscordTalkState discordTalkState)
        {
            this.pathOptions = pathOptions.Value;
            this.discordAudioClient = discordAudioClient;
            this.discordWebhookClient = discordWebhookClient;
            this.discordTalkState = discordTalkState;
        }

        public void PlayMusic(SocketCommandContext context, string path)
        {
            var channel = context.Guild.VoiceChannels.MaxUserJoined();
            if (channel == null)
            {
                return;
            }
            if (string.IsNullOrEmpty(path))
            {
                path = "kongyo.wav";
            }
            else
            {
                path = Path.GetFileName(path);
            }
            path = Path.Combine(pathOptions.MusicDir, path);
            discordAudioClient.EnqueueRawFile(channel, path);
        }

        public string PlayFortune(SocketCommandContext context)
        {
            var files = Directory.GetFiles(pathOptions.FortuneDir, "*.png");
            return files[new Random().Next(files.Length)];
        }

        public async Task PlayDanceAsync(SocketCommandContext context)
        {
            var savedMessageId = discordTalkState.PrevMessageId;

            await discordWebhookClient.SendMessageAsync(
                    "なんか静かですねぇ。街の中にはギャラルホルンもいないし本部とはえらい違いだ。",
                    "ライド・マッス",
                    "https://cdn.discordapp.com/attachments/397946628394319874/426237499338129410/31ab612050ddc4a0.png"
                );
            await Task.Delay(2000);
            if (savedMessageId != discordTalkState.PrevMessageId)
                return;

            await discordWebhookClient.SendMessageAsync(
                "ああ。火星の戦力は軒並み向こうに回してんのかもな。",
                "オルガ・イツカ",
                "https://cdn.discordapp.com/attachments/397946628394319874/426238682194771968/d006e5429eb70a8f.png"
            );
            await Task.Delay(2000);
            if (savedMessageId != discordTalkState.PrevMessageId)
                return;

            await discordWebhookClient.SendMessageAsync(
                "まっ、そんなのもう関係ないですけどね！",
                "ライド・マッス",
                "https://cdn.discordapp.com/attachments/397946628394319874/426237499338129410/31ab612050ddc4a0.png"
            );
            await Task.Delay(2000);
            if (savedMessageId != discordTalkState.PrevMessageId)
                return;

            await discordWebhookClient.SendMessageAsync(
                "上機嫌だな。",
                "オルガ・イツカ",
                "https://cdn.discordapp.com/attachments/397946628394319874/426238682194771968/d006e5429eb70a8f.png"
            );
            await Task.Delay(2000);
            if (savedMessageId != discordTalkState.PrevMessageId)
                return;

            await discordWebhookClient.SendMessageAsync(
                "そりゃそうですよ！みんな助かるし、タカキも頑張ってたし、俺も頑張らないと！",
                "ライド・マッス",
                "https://cdn.discordapp.com/attachments/397946628394319874/426237499338129410/31ab612050ddc4a0.png"
            );
            await Task.Delay(2000);
            if (savedMessageId != discordTalkState.PrevMessageId)
                return;

            await discordWebhookClient.SendMessageAsync(
                "ああ、そうだ（詠唱開始）",
                "オルガ・イツカ",
                "https://cdn.discordapp.com/attachments/397946628394319874/426240322687991809/e9558ca6d3de6eb5.png"
            );
            await Task.Delay(2000);

            await discordWebhookClient.SendMessageAsync(
                "俺たちが今まで積み上げてきたもんは全部無駄じゃなかった。これからも俺たちが立ち止まらないかぎり道は続く・・・。",
                "オルガ・イツカ",
                "https://cdn.discordapp.com/attachments/397946628394319874/426240322687991809/e9558ca6d3de6eb5.png"
            );
            await Task.Delay(2000);

            await discordWebhookClient.SendMessageAsync(
                "ぐわっ！",
                "チャド・チャダーン",
                "https://cdn.discordapp.com/attachments/397946628394319874/426241675032133632/c9815ddd02f7561b.png"
            );
            await Task.Delay(2000);

            await discordWebhookClient.SendMessageAsync(
                "団長！何やってんだよ！団長！",
                "ライド・マッス",
                "https://cdn.discordapp.com/attachments/397946628394319874/426243051451580429/8c065bd78d692d93.png"
            );
            await Task.Delay(2000);

            await discordWebhookClient.SendMessageAsync(
                "ぐっ！うおぉ～～！",
                "オルガ・イツカ",
                "https://cdn.discordapp.com/attachments/397946628394319874/426242200007999499/3229514d7b85b20a.png"
            );
            await Task.Delay(2000);

            await discordWebhookClient.SendMessageAsync(
                "なんだよ・・・結構当たんじゃねぇか・・・。",
                "オルガ・イツカ",
                "https://cdn.discordapp.com/attachments/397946628394319874/426242648471502850/dffb9c0252ffbfcf.png"
            );
            await Task.Delay(2000);

            await discordWebhookClient.SendMessageAsync(
                "だ・・・団長・・・。あっ・・・あぁ・・・。",
                "ライド・マッス",
                "https://cdn.discordapp.com/attachments/397946628394319874/426243051451580429/8c065bd78d692d93.png"
            );
            await Task.Delay(2000);

            await discordWebhookClient.SendMessageAsync(
                "なんて声出してやがる・・・ライドォ！",
                "オルガ・イツカ",
                "https://cdn.discordapp.com/attachments/397946628394319874/426243612272230400/8f815093f25a98ee.png"
            );
            await Task.Delay(2000);

            await discordWebhookClient.SendMessageAsync(
                "だって・・・だって・・・。",
                "ライド・マッス",
                "https://cdn.discordapp.com/attachments/397946628394319874/426248463806758922/2b3c7b7c38a82299.png"
            );
            await Task.Delay(2000);

            await discordWebhookClient.SendMessageAsync(
                "俺は鉄華団団長オルガ・イツカだぞ。こんくれぇなんてこたぁねぇ。",
                "オルガ・イツカ",
                "https://cdn.discordapp.com/attachments/397946628394319874/426244046755987456/b2a0429e340c1efa.png"
            );
            await Task.Delay(2000);

            await discordWebhookClient.SendMessageAsync(
                "そんな・・・俺なんかのために・・・。",
                "ライド・マッス",
                "https://cdn.discordapp.com/attachments/397946628394319874/426248463806758922/2b3c7b7c38a82299.png"
            );
            await Task.Delay(2000);

            await discordWebhookClient.SendMessageAsync(
                "団員を守んのは俺の仕事だ。",
                "オルガ・イツカ",
                "https://cdn.discordapp.com/attachments/397946628394319874/426244286863114242/f59bb2a3daf4f1ad.png"
            );
            await Task.Delay(2000);

            await discordWebhookClient.SendMessageAsync(
                "でも！",
                "ライド・マッス",
                "https://cdn.discordapp.com/attachments/397946628394319874/426248463806758922/2b3c7b7c38a82299.png"
            );
            await Task.Delay(2000);

            await discordWebhookClient.SendMessageAsync(
                "いいから行くぞ！皆が待ってんだ。それに・・・。ミカ、やっと分かったんだ。",
                "オルガ・イツカ",
                "https://cdn.discordapp.com/attachments/397946628394319874/426245629522739210/921eace1b2a54498.png"
            );
            await Task.Delay(2000);

            await discordWebhookClient.SendMessageAsync(
                "俺たちにはたどりつく場所なんていらねぇ。ただ進み続けるだけでいい。止まんねぇかぎり、道は続く。",
                "オルガ・イツカ",
                "https://cdn.discordapp.com/attachments/397946628394319874/426245629522739210/921eace1b2a54498.png"
            );
            await Task.Delay(2000);

            await discordWebhookClient.SendMessageAsync(
                "謝ったら許さない。",
                "三日月・オーガス",
                "https://cdn.discordapp.com/attachments/397946628394319874/426246050085601280/82d3e573260a0cb7.png"
            );
            await Task.Delay(2000);

            await discordWebhookClient.SendMessageAsync(
                "ああ、分かってる。",
                "オルガ・イツカ",
                "https://cdn.discordapp.com/attachments/397946628394319874/426246453036318730/32cc8d3504ce6ca5.png"
            );
            await Task.Delay(2000);

            await discordWebhookClient.SendMessageAsync(
                "俺は止まんねぇからよ、お前らが止まんねぇかぎり、その先に俺はいるぞ！",
                "オルガ・イツカ",
                "https://cdn.discordapp.com/attachments/397946628394319874/426246841512755220/fee270be4ec3698b.png"
            );
            await Task.Delay(10000);

            await discordWebhookClient.SendMessageAsync(
                "だからよ、止まるんじゃねぇぞ・・・。",
                "オルガ・イツカ",
                "https://cdn.discordapp.com/attachments/397946628394319874/426247529605365771/bac0b8d3dc7c68e4.png"
            );
        }
    }
}
