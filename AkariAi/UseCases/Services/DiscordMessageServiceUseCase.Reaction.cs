﻿using Discord;
using Discord.WebSocket;
using System;
using System.Linq;
using System.Threading.Tasks;
using Vbot.Extensions;

namespace Vbot.UseCases.Services
{
    public partial class DiscordMessageServiceUseCase
    {
        private async Task ReactionAdded(Cacheable<IUserMessage, ulong> message, ISocketMessageChannel channel, SocketReaction reaction)
        {
            if (!reaction.User.IsSpecified)
            {
                return;
            }
            var user = reaction.User.Value;
            if (user.IsBot)
            {
                return;
            }

            if (reaction.Emote.Name == "\U0001F4A9")
            {
                IUserMessage m;
                if (message.HasValue)
                {
                    m = message.Value;
                }
                else
                {
                    m = await channel.GetMessageAsync(message.Id) as IUserMessage;
                }
                if (m == null)
                {
                    return;
                }

                if (user is SocketGuildUser fromUser && m.Author is SocketGuildUser toUser)
                {
                    var role = fromUser.Guild.Roles.Get("うんこ");
                    if (role == null)
                    {
                        return;
                    }
                    if (!fromUser.Roles.Contains(role))
                    {
                        return;
                    }
                    await toUser.AddRoleAsync(role);
                }
            }
        }
    }
}
