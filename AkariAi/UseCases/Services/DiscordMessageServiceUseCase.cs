﻿using Microsoft.Extensions.Logging;
using System;
using System.Reactive.Disposables;
using System.Threading;
using System.Threading.Tasks;
using Vbot.Clients;
using Vbot.Extensions;
using Vbot.Models;

namespace Vbot.UseCases.Services
{
    public partial class DiscordMessageServiceUseCase : IDiscordMessageServiceUseCase
    {
        private readonly ILogger logger;
        private readonly IDiscordMessageClient discordClient;
        private readonly IDiscordWebhookClient discordWebhookClient;
        private readonly ITalkClient talkClient;
        private readonly DiscordTalkState discordTalkState;

        public DiscordMessageServiceUseCase(
            ILogger<DiscordMessageServiceUseCase> logger,
            IDiscordMessageClient discordClient,
            IDiscordWebhookClient discordWebhookClient,
            ITalkClient talkClient,
            DiscordTalkState discordTalkState)
        {
            this.logger = logger;
            this.discordClient = discordClient;
            this.discordWebhookClient = discordWebhookClient;
            this.talkClient = talkClient;
            this.discordTalkState = discordTalkState;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            observers = new CompositeDisposable(
                discordClient.Ready.Subscribe(Ready, OnError),
                discordClient.MessageReceived.SubscribeAsync(MessageReceived, OnError),
                discordClient.MessageUpdated.SubscribeAsync(MessageUpdated, OnError),
                discordClient.ReactionAdded.SubscribeAsync(ReactionAdded, OnError));
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            observers?.Dispose();
            return Task.CompletedTask;
        }

        public Task ExecuteAsync(CancellationToken cancellationToken)
        {
            return ExecuteActivityAsync(cancellationToken);
        }

        private void OnError(Exception ex)
        {
            logger.LogError(ex, ex.Message);
        }
    }
}
