﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Vbot.UseCases.Services
{
    public partial class DiscordMessageServiceUseCase
    {
        private const int IntervalChangeActivityMilliseconds = 1800000;

        private static readonly string[] ActivityList = {
            "KITCHEN",
            "斎藤さん",
            "VRChat",
            "どうぶつタワーバトル",
            "Getting Over It with Bennett Foddy",
            "おえかきの森",
            "Undertale",
            "QUICK,DRAW!",
            "Airtone",
            "Until Dawn: Rush of Blood",
            "SEIYA",
            "マリオカート",
            "セガキャッチャーオンライン",
            "Beat Saber",
            "Dead Hungry",
            "ねこあつめ",
            "Trollface Quest",
            "ラジオ体操",
        };

        private Task ChangeDiscordActivity()
        {
            var random = new Random();
            return discordClient.SetActivityAsync(ActivityList[random.Next(ActivityList.Length)]);
        }

        private async Task ExecuteActivityAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                await Task.Delay(IntervalChangeActivityMilliseconds, cancellationToken);
                await ChangeDiscordActivity();
            }
        }
    }
}
