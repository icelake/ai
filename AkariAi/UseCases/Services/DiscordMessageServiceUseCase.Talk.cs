﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Reactive;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Vbot.Consts;
using Vbot.Extensions;

namespace Vbot.UseCases.Services
{
    public partial class DiscordMessageServiceUseCase
    {
        private static readonly string[] GreetingMorning = {
            "おはよう :sunny:",
            "おはようございます :blush: :heartpulse:",
            "おはよう＞ω＜/",
            "おはよ( •̀ᴗ•́ )/",
            "おはよおおおおおおー！！！！",
            "おはようOo(っд･｀｡)",
            "おはよ∠(*°ω°)／ :sunny: :sunny:",
        };
        private static readonly string[] GreetingNight = {
            "おやすみ",
            "おやすみ〜(*ˊᵕˋ*)੭",
            "おやすみー( •̀ᴗ•́ )/",
            "おやすみー( •̀ᴗ•́ )/",
            "おやすみー(-_-)zzz",
            "おやすみ〜(><)",
            "おやすみでーす(><)",
            "おやすみですー(ノ´∀｀)ノ",
            "おやすみ :wink: :notes:",
            "おやすみ〜 :sleepy:",
            "おやすみ〜 :sleeping:",
        };
        private static readonly string[] GreetingCongrats = {
            "おめっと(｡•̀ᴗ-)✧",
            "おめでとぉぉお！",
            "おぉ！おめです(><)",
            "おめです(><)",
            "おめでとです(●´ω`●) :heartbeat:",
        };
        private static readonly string[] GreetingPiggyback = {
            "そうだそうだ(｡•̀ᴗ-)✧",
            "そうだよ :thumbsup:",
            "そうだそうだ！(`‐ω‐´)",
            "そうだぞ！",
            "そうだよ :heart:",
        };
        private static readonly string[] CheerMessage = {
            "がんば :thumbsup: :heart:️",
            "がんばぁぁぁ :right_facing_fist:",
            "ガンバ(๑•́ω•̀๑)",
            "頑張って٩(๑>∀<๑)۶",
            "頑張ってぇぇ :joy: :thumbsup: :thumbsup:",
            "頑張って(`‐ω‐´)",
            "頑張ってฅ•ω•ฅ",
            "頑張って :muscle: (^~^)",
            "頑張ってね( ☉_☉)",
            "頑張って︎ :muscle:",
            "頑張ってね :confounded: :heartbeat:",
            "頑張ってぇぇええ٩(ˊᗜˋ*)و :heartbeat:",
            "頑張って٩(ˊᗜˋ*)و",
            "頑張って(๑´ω`)ﾉ",
            "頑張れー(ง •̀_•́)ง",
            "頑張れー٩(๑>∀<๑)۶ :hearts: Fight :hearts:"
        };
        private static readonly (string, string)[] RewritePair = {
            ("にてる", "にてない"),
            ("似てる", "似てない"),
            ("つよそう", "よわそう"),
            ("強そう", "弱そう"),
            ("つよい", "よわい"),
            ("強い", "弱い"),
            ("おいしそう", "まずそう"),
            ("いる", "いらない"),
            ("きれい", "きたない"),
            ("すき", "きらい"),
            ("かなしい", "うれしい"),
            ("悲しい", "嬉しい"),
        };

        private static readonly Regex RegexPoo = new Regex(@"(?:うん[ちこ]|大便|" + "\U0001F4A9" + @")", RegexOptions.Compiled);
        private static readonly Regex RegexAkari = new Regex(@"(?:みらい|あかり|mirai|akari)", RegexOptions.Compiled);
        private static readonly Regex RegexOrg = new Regex(@"(?:おるが|いつか|団長)", RegexOptions.Compiled);
        private static readonly Regex RegexCheer = new Regex(@"(?:(?:がんば|頑張)(?:えー|っ?てら?|れ|る)?(?:!|！)*)$", RegexOptions.Compiled);

        private class InvisibleState : IDisposable
        {
            public bool State { get; private set; } = false;

            public InvisibleState Enter()
            {
                State = true;
                return this;
            }

            public void Dispose()
            {
                State = false;
            }
        }

        private readonly InvisibleState invisibleState = new InvisibleState();

        private IDisposable observers = null;
        private DateTime lastGreetingMorningAnswered = DateTime.MinValue;
        private DateTime lastGreetingNightAnswered = DateTime.MinValue;
        private DateTime lastGreetingCongratsAnswered = DateTime.MinValue;
        private DateTime lastCheerAnswered = DateTime.MinValue;
        private DateTime lastPiggybackAnswered = DateTime.MinValue;
        private DateTime lastGeneralAnswered = DateTime.MinValue;
        private DateTime lastMessageAnswered = DateTime.MinValue;

        private ulong prevUserId = 0;
        private string prevMessage = null;
        private string lastMessage = null;

        private async void Ready(Unit unit)
        {
            await ChangeDiscordActivity();
        }

        private async Task MessageReceived(SocketUserMessage message)
        {
            if (await discordClient.ExecuteCommandAsync(message))
            {
                return;
            }

            if (invisibleState.State)
            {
                return;
            }

            var content = message.Content;
            if (string.IsNullOrEmpty(content))
            {
                return;
            }

            int argPos = 0;
            var isMentionToMe = message.HasMentionPrefix(discordClient.CurrentUser, ref argPos);
            if (isMentionToMe)
            {
                content = content.Substring(argPos);
            }
            var isPrivateChannel = message.Channel is IPrivateChannel;

            var sanitizedContent = content.SanitizeMessage();
            if (string.IsNullOrEmpty(sanitizedContent))
            {
                return;
            }

            if (RegexPoo.IsMatch(sanitizedContent)) //うんち
            {
                if (message.Author is IGuildUser user)
                {
                    discordClient.AddReactionQueue(message, EmojiConsts.Poo);
                    discordClient.AddRoleQueue(user, "うんこ");
                }
            }

            if (RegexOrg.IsMatch(sanitizedContent)) //団長
            {
                discordClient.AddReactionQueue(message, EmojiConsts.Org);
            }

            if (message.Author.IsBot)
            {
                return;
            }

            discordTalkState.PrevMessageId = message.Id;

            if (RegexAkari.IsMatch(sanitizedContent)) //アカリ
            {
                discordClient.AddReactionQueue(message, EmojiConsts.Akari);
            }

            var random = new Random();

            if (sanitizedContent.StartsWith("おは") || sanitizedContent.StartsWith("おふぁ")) //朝
            {
                if (lastGreetingMorningAnswered.AddMinutes(3) <= DateTime.Now || isMentionToMe || isPrivateChannel)
                {
                    lastGreetingMorningAnswered = DateTime.Now;
                    if (discordWebhookClient.IsTargetChannel(message.Channel))
                    {
                        switch (random.Next(50))
                        {
                            case 0:
                                discordWebhookClient.SendMessageQueue(
                                    "おはYUA～",
                                    "YUA",
                                    "https://pbs.twimg.com/profile_images/909710799282647041/7wTx03IU_400x400.jpg"
                                );
                                return;
                        }
                    }
                    discordClient.SendMessageQueue(message.Channel, GreetingMorning[random.Next(GreetingMorning.Length)]);
                }
                return;
            }

            if (sanitizedContent.StartsWith("おやす")) //夜
            {
                if (lastGreetingNightAnswered.AddMinutes(3) <= DateTime.Now || isMentionToMe || isPrivateChannel)
                {
                    lastGreetingNightAnswered = DateTime.Now;
                    discordClient.SendMessageQueue(message.Channel, GreetingNight[random.Next(GreetingNight.Length)]);

                    if (random.Next(50) == 0)
                    {
                        var t = Task.Run(async () =>
                        {
                            using (invisibleState.Enter())
                            {
                                await Task.Delay(5000);
                                discordClient.SendMessageQueue(message.Channel, "アカリも寝ます :sleepy:");
                                await Task.Delay(2000);
                                discordClient.SendMessageQueue(message.Channel, GreetingNight[random.Next(GreetingNight.Length)]);
                                await Task.Delay(2000);
                                try
                                {
                                    await discordClient.SetStatusAsync(UserStatus.Invisible);
                                    await Task.Delay(1800000);
                                }
                                finally
                                {
                                    await discordClient.SetStatusAsync(UserStatus.Online);
                                }
                            }
                        });
                    }
                }
                return;
            }

            if (sanitizedContent.StartsWith("おめで")) //おめでとう
            {
                if (lastGreetingCongratsAnswered.AddMinutes(3) <= DateTime.Now || isMentionToMe || isPrivateChannel)
                {
                    lastGreetingCongratsAnswered = DateTime.Now;
                    discordClient.SendMessageQueue(message.Channel, GreetingCongrats[random.Next(GreetingCongrats.Length)]);
                }
                return;
            }

            if (RegexCheer.IsMatch(sanitizedContent)) // がんばれ
            {
                if (lastCheerAnswered.AddMinutes(3) <= DateTime.Now || isMentionToMe || isPrivateChannel)
                {
                    lastCheerAnswered = DateTime.Now;
                    discordClient.SendMessageQueue(message.Channel, CheerMessage[random.Next(CheerMessage.Length)]);
                }
                return;
            }

            if (sanitizedContent.StartsWith("はろー") && !sanitizedContent.StartsWith("はろーわーく")) //ハロー
            {
                if (lastGeneralAnswered.AddMinutes(3) <= DateTime.Now || isMentionToMe || isPrivateChannel)
                {
                    lastGeneralAnswered = DateTime.Now;
                    if (discordWebhookClient.IsTargetChannel(message.Channel))
                    {
                        switch (random.Next(50))
                        {
                            case 0:
                                discordWebhookClient.SendMessageQueue(
                                    "ハロー旦那様\nアナタの嫁の萌美だよ",
                                    "萌実",
                                    "https://pbs.twimg.com/profile_images/899606785496989698/6KU-X_SG_400x400.jpg"
                                );
                                return;
                            case 1:
                                discordWebhookClient.SendMessageQueue(
                                    "ハローダーリン！",
                                    "ヨメミ",
                                    "https://pbs.twimg.com/profile_images/944140012001533958/8rXDgq6z_400x400.jpg"
                                );
                                return;
                        }
                    }
                    discordClient.SendMessageQueue(message.Channel, "ハロー！ミライアカリだよ！(ﾋﾟﾛﾘﾝ)");
                }
                return;
            }

            if (sanitizedContent.StartsWith("にゃんはろ")) //ニャンハロー
            {
                if (lastGeneralAnswered.AddMinutes(3) <= DateTime.Now || isMentionToMe || isPrivateChannel)
                {
                    lastGeneralAnswered = DateTime.Now;
                    if (discordWebhookClient.IsTargetChannel(message.Channel))
                    {
                        switch (random.Next(50))
                        {
                            case 0:
                                discordWebhookClient.SendMessageQueue(
                                    "ハロー旦那様\nアナタの嫁の萌美だよ",
                                    "萌実",
                                    "https://pbs.twimg.com/profile_images/899606785496989698/6KU-X_SG_400x400.jpg"
                                );
                                return;
                            case 1:
                                discordWebhookClient.SendMessageQueue(
                                    "ハローダーリン！",
                                    "ヨメミ",
                                    "https://pbs.twimg.com/profile_images/944140012001533958/8rXDgq6z_400x400.jpg"
                                );
                                return;
                            default:
                                discordWebhookClient.SendMessageQueue(
                                    "ニャンハロー",
                                    "エイレーン (Eilene)",
                                    "https://pbs.twimg.com/profile_images/443553226583855104/J6w3vQi0_400x400.jpeg"
                                );
                                return;
                        }
                    }
                    discordClient.SendMessageQueue(message.Channel, "ハロー！ミライアカリだよ！(ﾋﾟﾛﾘﾝ)");
                }
                return;
            }

            if ((sanitizedContent.StartsWith("みらいあかりも") || sanitizedContent.StartsWith("あかりちゃんも")) && (sanitizedContent.EndsWith("てる") || sanitizedContent.EndsWith("いる")))
            {
                if (lastPiggybackAnswered.AddMinutes(3) <= DateTime.Now || isMentionToMe || isPrivateChannel)
                {
                    lastPiggybackAnswered = DateTime.Now;
                    discordClient.SendMessageQueue(message.Channel, GreetingPiggyback[random.Next(GreetingPiggyback.Length)]);
                }
                return;
            }

            if (sanitizedContent.EndsWith("送るよ") || sanitizedContent.EndsWith("送るよ?") || sanitizedContent.EndsWith("あげるね") || sanitizedContent.EndsWith("あげるね?"))
            {
                discordClient.SendMessageQueue(message.Channel, $"<@{message.Author.Id}> いらない");
                return;
            }

            if (sanitizedContent.EndsWith("食べる?") || sanitizedContent.EndsWith("たべる?") || sanitizedContent.EndsWith("いる?"))
            {
                //lastPostedMessage = DateTime.Now;
                if (sanitizedContent.Contains("ちーずばーがー") || sanitizedContent.Contains("牛丼"))
                    discordClient.SendMessageQueue(message.Channel, $"<@{message.Author.Id}> いらない");
                else if (sanitizedContent.Contains("だんごむし"))
                    discordClient.SendMessageQueue(message.Channel, $"<@{message.Author.Id}> ひどい");
                else if (random.Next(2) == 0)
                    discordClient.SendMessageQueue(message.Channel, $"<@{message.Author.Id}> いる");
                else if (sanitizedContent.EndsWith("ている?"))
                    discordClient.SendMessageQueue(message.Channel, $"<@{message.Author.Id}> いない");
                else
                    discordClient.SendMessageQueue(message.Channel, $"<@{message.Author.Id}> いらない");
                return;
            }

            if (content.EndsWith("にてるか?") || content.EndsWith("似てるか?"))
            {
                //lastPostedMessage = DateTime.Now;
                if (random.Next(2) == 0)
                    discordClient.SendMessageQueue(message.Channel, $"<@{message.Author.Id}> にてる");
                else
                    discordClient.SendMessageQueue(message.Channel, $"<@{message.Author.Id}> にてない");
                return;
            }

            if (sanitizedContent.EndsWith("すき?") || sanitizedContent.EndsWith("好き?"))
            {
                //lastPostedMessage = DateTime.Now;
                if (sanitizedContent.Contains("ちーずばーがー"))
                    discordClient.SendMessageQueue(message.Channel, $"<@{message.Author.Id}> すき");
                else if (random.Next(2) == 0)
                    discordClient.SendMessageQueue(message.Channel, $"<@{message.Author.Id}> すき");
                else
                    discordClient.SendMessageQueue(message.Channel, $"<@{message.Author.Id}> きらい");
                return;
            }

            if (sanitizedContent.Contains("だんごむし") && (sanitizedContent.EndsWith("食え") || sanitizedContent.EndsWith("くえ")))
            {
                discordClient.SendMessageQueue(message.Channel, $"<@{message.Author.Id}> ひどい");
                return;
            }

            if (isMentionToMe)
            {
                var resMes = await talkClient.TalkAsync(content, message.Author.Username);
                discordClient.SendMessageQueue(message.Channel, $"<@{message.Author.Id}> {resMes}");
                return;
            }
            if (isPrivateChannel)
            {
                var resMes = await talkClient.TalkAsync(content, message.Author.Username);
                discordClient.SendMessageQueue(message.Channel, resMes);
                return;
            }

            // echo
            if (prevUserId != message.Author.Id)
            {
                if (prevMessage != null && prevMessage.Length < 8 && content.Length < 8 &&
                    ((content.StartsWith("こん") && prevMessage.StartsWith("こん")) || (content.StartsWith("おか") && prevMessage.StartsWith("おか") || (content.Contains("てら") && prevMessage.Contains("てら")))))
                {
                    if (lastGeneralAnswered.AddMinutes(3) <= DateTime.Now)
                    {
                        discordClient.SendMessageQueue(message.Channel, prevMessage);
                        lastGeneralAnswered = DateTime.Now;
                        prevUserId = 0;
                        prevMessage = null;
                    }
                    return;
                }

                if (content == prevMessage)
                {
                    var rewritedContent = content;
                    if ((lastMessage == content) && lastMessageAnswered.AddMinutes(3) > DateTime.Now)
                    {
                        return;
                    }

                    if (random.Next(2) == 0)
                    {
                        foreach (var pair in RewritePair)
                        {
                            if (content.EndsWith(pair.Item1))
                            {
                                rewritedContent = content.Substring(0, content.Length - pair.Item1.Length) + pair.Item2;
                            }
                            else if (content.EndsWith(pair.Item2))
                            {
                                rewritedContent = content.Substring(0, content.Length - pair.Item2.Length) + pair.Item1;
                            }
                        }
                    }

                    lastMessage = content;
                    discordClient.SendMessageQueue(message.Channel, rewritedContent);
                    lastMessageAnswered = DateTime.Now;
                    prevUserId = 0;
                    prevMessage = null;
                    return;
                }
            }
            prevUserId = message.Author.Id;
            prevMessage = content;
        }

        private Task MessageUpdated(Cacheable<IMessage, ulong> oldMessage, SocketMessage socketMessage, ISocketMessageChannel channel)
        {
            if (invisibleState.State)
            {
                return Task.CompletedTask;
            }

            var message = socketMessage as SocketUserMessage;
            if (message == null)
            {
                return Task.CompletedTask;
            }

            var content = message.Content;
            if (string.IsNullOrEmpty(content))
            {
                return Task.CompletedTask;
            }

            var sanitizedContent = content.SanitizeMessage();
            if (string.IsNullOrEmpty(sanitizedContent))
            {
                return Task.CompletedTask;
            }

            if (RegexPoo.IsMatch(sanitizedContent)) //うんち
            {
                if (message.Author is IGuildUser user)
                {
                    discordClient.AddReactionQueue(message, EmojiConsts.Poo);
                    discordClient.AddRoleQueue(user, "うんこ");
                }
            }

            return Task.CompletedTask;
        }
    }
}
