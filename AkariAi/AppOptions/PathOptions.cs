﻿namespace Vbot.AppOptions
{
    public class PathOptions
    {
        public string MusicDir { get; set; }
        public string FortuneDir { get; set; }
    }
}
